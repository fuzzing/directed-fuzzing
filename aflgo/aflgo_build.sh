#!/bin/bash -ue

export SUBJECT="$( readlink -f ${1} )"
export TMP_DIR=$PWD/temp

export CC=afl-clang-fast
export CXX=afl-clang-fast++
export COPY_CFLAGS=$CFLAGS
export COPY_CXXFLAGS=$CXXFLAGS
export ADDITIONAL="-targets=$TMP_DIR/BBtargets.txt -outdir=$TMP_DIR -flto -fuse-ld=gold -Wl,-plugin-opt=save-temps"
export CFLAGS="$CFLAGS $ADDITIONAL"
export CXXFLAGS="$CXXFLAGS $ADDITIONAL"

STEP1() {
  export LDFLAGS=-lpthread
  pushd $SUBJECT
    autoreconf -fi
#   ./autogen.sh
    ./configure --disable-shared >/dev/null
    make clean >/dev/null
    make all
  popd >/dev/null
}


# $SUBJECT/xmllint --valid --recover $SUBJECT/test/dtd3

# ls $TMP_DIR/dot-files

STEP2() {
  # Clean up
  cat $TMP_DIR/BBnames.txt | rev | cut -d: -f2- | rev | sort | uniq > $TMP_DIR/BBnames2.txt && mv $TMP_DIR/BBnames2.txt $TMP_DIR/BBnames.txt
  cat $TMP_DIR/BBcalls.txt | sort | uniq > $TMP_DIR/BBcalls2.txt && mv $TMP_DIR/BBcalls2.txt $TMP_DIR/BBcalls.txt
}

STEP3() {
  # Generate distance ☕️
  $AFLGO/scripts/genDistance.sh $SUBJECT $TMP_DIR xmllint
}



STEP4() {
  read -p "[*] Continue to instrument subject ($SUBJECT)? " c
  export CFLAGS="$COPY_CFLAGS -distance=$TMP_DIR/distance.cfg.txt"
  export CXXFLAGS="$COPY_CXXFLAGS -distance=$TMP_DIR/distance.cfg.txt"
  
  # Clean and build subject with distance instrumentation ☕️
  pushd $SUBJECT
    make clean
      ./configure --disable-shared
        make all
  popd >/dev/null

}

STATUS() {
  # Print extracted targets. 
  echo "BB Targets:"
  if [[ -e $TMP_DIR/BBtargets.txt ]]; then 
      cat $TMP_DIR/BBtargets.txt
  fi
  
  printf  "\nFunction targets:\n"
  if [[ -e $TMP_DIR/Ftargets.txt ]]; then 
    cat $TMP_DIR/Ftargets.txt
  fi
  
  # Check distance file
  printf  "\nDistance values:\n"
  if [[ -e $TMP_DIR/distance.cfg.txt ]]; then
      head -n5 $TMP_DIR/distance.cfg.txt
      echo "..."
      tail -n5 $TMP_DIR/distance.cfg.txt
  fi
}

${2:-STATUS}
