#!/bin/bash

rm -rf raw
mkdir raw
export LLVM_PROFILE_FILE="raw/test-%m.profraw"
i=001

mkdir -p out
rm -f out/*

for f in \
Azzz \
ABzz \
ABCz \
ABCD \
ABCD123 \
ABCD1234 \
ABCD1234EFGH \
ABCD1234EFGHIJKL \
Hzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
hzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
Azzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCDzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCD1234zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCD1234EFGHzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCD1234EFGHIJKLzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
ABCD1234EFGHIJKLDEADBEEFzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
HBCD123zEFGHIJKLDEADBEEFzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzi \
ABCD1234EFGHIJKLDEADBEEFAAAAAAAAAAAAIJKLAAA \
ABCD1234EFGHIJKLDEADBEEF \
ABCD1234EFGHIJKLDEADBEEF \
HI! \
ABCD123zEFzHIJKLDEADBEEFzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz \
;do 
printf -v j "%03d" $i
printf "%s" $f > out/${j}

i=$(( i + 1 ))

done
printf "DONE!                  \n"

# ./test-cmplog-libfuzzer -dump_coverage=1 out
