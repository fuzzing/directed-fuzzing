#include <stdio.h>
#include <stdint.h>

#include "khash.h"

KHASH_MAP_INIT_INT64(m64, char)      // instantiate structs and methods

KHASH_SET_INIT_INT64(s64)

int main() {
  int absent, is_missing;
  int v;
  uint64_t edge;
  khint_t k, size;
  khash_t(m64) *h = kh_init(m64);  // allocate a hash table

  khash_t(s64) *s = kh_init(s64);  // allocate a set

  for (v = 0; v < 6; v++) {
    k = kh_put(m64, h, v, &absent);  // insert a key to the hash table
    size = kh_size(h);
    fprintf(stderr, "  inserted %d, absent = %d, k = %x, size=%d\n", v, absent, k, size);
    if (!absent) {
	    kh_del(m64, h, k);
	    fprintf(stderr, "  NOT ABSENT?\n");
    }
    kh_value(h, k) = 10;             // set the value
    kh_put(s64, s, v,  &absent);
  }
  v = 3;
  k = kh_put(m64, h, v, &absent);  // insert a key to the hash table
  fprintf(stderr, "  inserted %d, absent = %d, k = %x\n", v, absent, k);
  fprintf(stderr, "  set contains %d items\n", kh_size(s));
  for (v = 0; v < 7; v++) {
    k = kh_put(m64, h, v, &absent);
    if (!absent) kh_value(h, k)++;
    else kh_value(h, k) = v;
  }
  for (v = 0; v < 7; v++) {
    k = kh_get(m64, h, v);
    fprintf(stderr, "  key = %d, value = %d\n", v, kh_value(h, k));
  }
 
  uint32_t src = 3;
  uint32_t dst = 11;
  edge = (uint64_t)src<<32 | dst;
  fprintf(stderr, " src = %u, dst = %u, edge = %lx (%lu)\n", src, dst, edge, edge);

  v = 10;
  k = kh_get(m64, h, v);          // query the hash table
  is_missing = (k == kh_end(h));   // test if the key is present
  fprintf(stderr, " queried %d, k= %x, is_missing= %d\n", v, k, is_missing);

  k = kh_get(m64, h, 5);
  fprintf(stderr, " queried %d, k= %x, \n", v, k);
  kh_del(m64, h, k);               // remove a key-value pair
  for (k = kh_begin(h); k != kh_end(h); ++k)  // traverse
    if (kh_exist(h, k))            // test if a bucket contains data
      kh_value(h, k) = 1;
  kh_destroy(m64, h);              // deallocate the hash table
  kh_destroy(s64, s);
  return 0;
}
