#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <sanitizer/dfsan_interface.h>
extern dfsan_label __pllr_dfsan_harness_label;


typedef struct callClass {
	int (*check2)(const uint8_t *data, size_t len);
} callClass;


char* reverse_section(char *s, size_t length) {
    if (length == 0) return s;

    size_t i; char temp;
    for (i = 0; i < length / 2 + 1; ++i)
        temp = s[i], s[i] = s[length - i], s[length - i] = temp;
    return s;
}

char* reverse_words_in_order(char *s, char delim) {
    if (!strlen(s)) return s;

    size_t i, j;
    for (i = 0; i < strlen(s) - 1; ++i) {
        for (j = 0; s[i + j] != 0 && s[i + j] != delim; ++j)
            ;
        reverse_section(s + i, j - 1);
        s += j;
    }
    return s;
}

char* reverse_string(char *s) {
    return strlen(s) ? reverse_section(s, strlen(s) - 1) : s;
}

int check_buf0(const uint8_t *data) {
  return data[0] == 'H' || data[0] == 'h';
}

int check_buf1(const uint8_t *data, size_t len) {
  reverse_string((char*)data);
  return data[0] == 'I' || data[0] == 'i';
}

int check_buf2(const uint8_t *data, size_t len) {
  return data[0] == 'E' || data[0] == 'e';
}

static callClass choose[] = {
	{
		check_buf1
	},
	{
		check_buf2
	}
};


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  int choice = 0;
  dfsan_set_label(__pllr_dfsan_harness_label, &choice, sizeof(choice));
  if (size > 0 && check_buf0(data)) {
    choice = data[0] == 'H';
    if (size > 1 && choose[choice].check2(data + 1, size - 1))
       if (size > 2 && data[2] == '!')
       __builtin_trap();
  }
  return 0;
}
