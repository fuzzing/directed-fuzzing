#include "llvm/IR/Instructions.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include "llvm/Support/BranchProbability.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Analysis/BranchProbabilityInfo.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

#define DEBUG_TYPE "path-llr"

const char CalculatePathLLRName []     = "__pllr_calculate_path_llr";
const char PathLLRCtorFunctionName []  = "__pllr_initialize";
const char PathLLRDtorFunctionName []  = "__pllr_finalize";
const char PathLLRFuncAddrsName []     = "__pllr_function_addrs";
const char PathLLRFunctionArrayName [] = "__pllr_function_names";
const char PathLLRNumFuncName []       = "__pllr_num_functions";
const char PathLLRTraceCallName []     = "__pllr_trace_call";

const char DFSanSetLabelFnName []      = "dfsan_set_label";

static cl::opt<bool>
UseHeuristic("use-heuristic", cl::init(false), cl::NotHidden,
             cl::desc("Use heuristic branch probabilities."));

static cl::opt<bool>
FullTrace("all-calls", cl::init(false), cl::NotHidden,
          cl::desc("Trace all calls (not just indirect ones)."));


static void createFunctionMap(Module &M, StringRef GlobalVarName, 
                              ArrayRef<Function*> Functions) {
  auto &CTX = M.getContext();
  uint64_t num_elems = Functions.size();
  IntegerType *Int64Ty = IntegerType::getInt64Ty(CTX);
  SmallVector<Constant *, 16> values;
  for (auto f : Functions) {
    Constant *FunctionPTR = ConstantExpr::getPtrToInt(f, Int64Ty);
    values.push_back(FunctionPTR);
#ifndef NDEBUG
    errs() << "  adding function " << f->getName() << format(" 0x%08lx", f) << "\n";
#endif
  }
  Constant *FunctionsArray = ConstantVector::get(values);
  GlobalVariable *newGlobalArray =
    new GlobalVariable(
      /*Module &        */ M,
      /*Type *          */ FunctionsArray->getType(),
      /* isConstant     */ true,
      /*Linkage         */ GlobalValue::ExternalLinkage,
      /*Initializer     */ FunctionsArray,
      /*Name            */ GlobalVarName
      /*InsertBefore    */
      /*ThreadLocalMode */
      /*AddressSpace    */
      /*isExternallyInitialized */
    );
    (void) newGlobalArray;
}


static Constant *createString(Module &M, LLVMContext &CTX,
                              StringRef data) {
  Constant *stringConstant = ConstantDataArray::getString(CTX, data);
  GlobalVariable *stringVar = 
    new GlobalVariable(
      /*Module &        */ M,
      /*Type *          */ stringConstant->getType(),
      /* isConstant     */ true,
      /*Linkage         */ GlobalValue::PrivateLinkage,
      /*Initializer     */ stringConstant
      /*Name            */ 
    );
  (void) stringVar;

  Constant *zero_32 = Constant::getNullValue(IntegerType::getInt32Ty(CTX));
  Constant *gep_params[] = { zero_32, zero_32 };
  Constant *functionName = 
    ConstantExpr::getGetElementPtr(stringVar->getValueType(), stringVar, gep_params);
  return functionName;
}


static void createFunctionNames(Module &M, StringRef GlobalVarName,
                                ArrayRef<Function*> Functions) {
  auto &CTX = M.getContext();
  uint64_t num_elems = Functions.size();
  PointerType *StringTy = IntegerType::getInt8PtrTy(CTX);

  SmallVector<Constant *> names;
  for (auto F : Functions) {
    auto name = createString(M, CTX, F->getName());
    names.push_back(name);
  }

  Constant *NamesArray = ConstantVector::get(names);
  GlobalVariable *newGlobalArray =
    new GlobalVariable(
      /*Module &        */ M,
      /*Type *          */ NamesArray->getType(),
      /* isConstant     */ true,
      /*Linkage         */ GlobalValue::ExternalLinkage,
      /*Initializer     */ NamesArray,
      /*Name            */ GlobalVarName
    );
    (void) assert(newGlobalArray && "Failed to create function names.");
}

static ConstantInt *getEdgeID(Instruction *TI, unsigned idx) {
  MDNode *WeightsNode =TI->getMetadata("pllr.edges");

  if (WeightsNode) {
    ConstantInt *Weight =
      mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(idx + 1));
    return Weight;
  }
  return NULL;
}

namespace llvm {

class InstrumentPathLLR {
  using MapType = DenseMap<uint32_t, double>;

private:
  Module *M;
  LLVMContext *C;
  const DataLayout *DL;
  BranchProbabilityInfo *curBPI;
  uint32_t cur_block_id = 0;
  uint32_t cur_edge_id = 0;
  IntegerType *Int16Ty;
  IntegerType *Int32Ty;
  IntegerType *Int64Ty;
  Type *DoubleTy;
  Type *IntptrTy;
  Type *VoidPtrTy;
  Type *VoidTy;
  GlobalVariable *NumEdges;
  GlobalVariable *NumFunctions;
  GlobalVariable *DFSanInputLabel;
  GlobalVariable *DFsanHarnessLabel;
  MapType EdgeWeights;
  FunctionCallee CalculatePathLLR;
  Function *PathLLRCtorFunction = nullptr;
  Function *PathLLRDtorFunction = nullptr;
  FunctionCallee TraceCallGraph;
  FunctionType *DFSanSetLabelFnTy;
  FunctionCallee DFSanSetLabelFn;

public:
  std::function<BranchProbabilityInfo *(Function &)> LookupBPI;

  InstrumentPathLLR(Module *M,
                    std::function<BranchProbabilityInfo *(Function &)> LookupBPI) {
    this->M = M;
    this->LookupBPI  = LookupBPI;
    instrumentModule();
  }

private:
  void instrumentModule() {
//  LLVMContext &C = M->getContext();
    C = &(M->getContext());
    DL = &M->getDataLayout();
    VoidTy = Type::getVoidTy(*C);
    Int16Ty = IntegerType::getInt16Ty(*C);
    Int32Ty = IntegerType::getInt32Ty(*C);
    Int64Ty = IntegerType::getInt64Ty(*C);
    DoubleTy = Type::getDoubleTy(*C);
    IntptrTy = Type::getIntNTy(*C, DL->getPointerSizeInBits());
    VoidPtrTy = Type::getInt8PtrTy(*C);

    double edge_weight;

    // Callgraph members
    SmallVector<Function*, 32> CallGraphFunctions;

    errs() << "[*] Instrumenting module " << M->getName() << "\n";

    NumEdges =
      new GlobalVariable(*M, Int64Ty, false, GlobalValue::ExternalLinkage, 0, "__pllr_total_edges");

    NumFunctions = 
      new GlobalVariable(*M, Int64Ty, true, GlobalValue::ExternalLinkage, 0, PathLLRNumFuncName);

    DFSanInputLabel =
      new GlobalVariable(*M, Int16Ty, true, GlobalValue::ExternalLinkage, 0, "__pllr_dfsan_input_label");

    DFsanHarnessLabel =
      new GlobalVariable(*M, Int16Ty, true, GlobalValue::ExternalLinkage, 0, "__pllr_dfsan_harness_label");

    CalculatePathLLR =
      M->getOrInsertFunction(CalculatePathLLRName, VoidTy, DoubleTy, Int32Ty, Int32Ty);
    
    TraceCallGraph = 
      M->getOrInsertFunction(PathLLRTraceCallName, VoidTy, Int64Ty, Int64Ty, Int32Ty);

    PathLLRCtorFunction =
      Function::Create(FunctionType::get(VoidTy, false),
                       GlobalValue::ExternalLinkage, PathLLRCtorFunctionName, M);
    PathLLRDtorFunction =
      Function::Create(FunctionType::get(VoidTy, false),
                       GlobalValue::ExternalLinkage, PathLLRDtorFunctionName, M);

    DFSanSetLabelFn =
      M->getOrInsertFunction(DFSanSetLabelFnName, VoidTy, Int16Ty, VoidPtrTy, Int64Ty);

    errs() << "[*] Identifying instructions to trace calls\n";    
    // Callgraph - identify functions in CG
    CallGraphFunctions.push_back(PathLLRCtorFunction);
    for (auto &F: *M) {
      if (!F.isIntrinsic())
        CallGraphFunctions.push_back(&F);
    }
    
    createFunctionMap(*M, PathLLRFuncAddrsName, CallGraphFunctions);
    createFunctionNames(*M, PathLLRFunctionArrayName, CallGraphFunctions);
    Constant *num_functions = ConstantInt::get(Int64Ty, CallGraphFunctions.size());
    NumFunctions->setInitializer(num_functions);

    for (auto &F: *M) {
      if (F.isDeclaration())
        continue;
#ifndef NDEBUG
      errs() << "  instrumenting function " << F.getName() << "\n";
#endif
      curBPI = LookupBPI(F);

      if (F.getName() == "LLVMFuzzerTestOneInput") {
        FunctionType *FT = F.getFunctionType();
        unsigned n = FT->getNumParams();
        labelFuzzerTestOneInputArgs(F);
      }

      for (auto &B : F) {
        for (auto &I : B) {
          if (CallBase *CB = dyn_cast<CallBase>(&I)) {
            if (Function *Callee = CB->getCalledFunction()) {
              // insert callgraph profile call
              if (FullTrace && !Callee->isIntrinsic())
                insert_call_trace(&I);
            } else {
              // Indirect call 
              insert_call_trace(&I);
            }
          }
        }
      }

      for (auto &B : F) {
        Instruction *TI = B.getTerminator();
	/* Skip invoke calls (always 100% probability) */
	if (isa<InvokeInst>(TI))
          continue;
        cur_block_id = cur_edge_id;
        if (TI && TI->getNumSuccessors() > 1) {
          for (unsigned I = 0, E = TI->getNumSuccessors(); I < E; I++) {
            edge_weight = getEdgeWeight(&B, TI, I);
            EdgeWeights[cur_edge_id] = edge_weight;
            cur_edge_id++;
          }
        }
      }
      curBPI->releaseMemory();
    }

    // Store the total number of weighted edges
    auto total_edges = ConstantInt::get(Int64Ty, cur_edge_id);
    NumEdges->setInitializer(total_edges);

    Triple TargetTriple = Triple(M->getTargetTriple());

    if (TargetTriple.isOSBinFormatELF()) {
      appendToGlobalCtors(*M, PathLLRCtorFunction, 1);
      appendToGlobalDtors(*M, PathLLRDtorFunction, 1);
    }
  }

  void SetNoSanitizeMetadata(Instruction *I) {
    I->setMetadata(I->getModule()->getMDKindID("nosanitize"),
                   MDNode::get(*C, None));
  }

  void labelFuzzerTestOneInputArgs(Function &F) {
    IRBuilder <> IRB(&F.front().front());
    LoadInst *LI = IRB.CreateLoad(DFSanInputLabel);
    CallInst *SetArg0 =
      IRB.CreateCall(DFSanSetLabelFn, {LI, F.getArg(0), F.getArg(1)});

    AllocaInst *AI = IRB.CreateAlloca(Int64Ty);
    StoreInst *SI = IRB.CreateStore(F.getArg(1), AI);
    Value *SizeOfArg1 = IRB.getInt64(8);
    Value *AddrOfSize = IRB.CreateBitCast(AI, VoidPtrTy);

    CallInst *SetArg1 =
      IRB.CreateCall(DFSanSetLabelFn, {LI, AddrOfSize, SizeOfArg1});
    SetNoSanitizeMetadata(LI);
    SetNoSanitizeMetadata(AI);
    SetNoSanitizeMetadata(SI);
    SetNoSanitizeMetadata(SetArg0);
    SetNoSanitizeMetadata(SetArg1);
  }

  void insert_call_trace(Instruction *I) {
    IRBuilder <> IRB(I);
    CallBase &CB = cast<CallBase>(*I);
    Value *Caller = CB.getCaller();
    Value *Callee = CB.getCalledOperand();
    if (isa<InlineAsm>(Callee))
      return;
#ifndef NDEBUG
    errs() << "  inserting function call trace for " << Callee->getName();
    errs() << " (" << Callee << ")\n";
#endif
    Value *CallerPtr = IRB.CreatePointerCast(Caller, IntptrTy);
    Value *CalleePtr = IRB.CreatePointerCast(Callee, IntptrTy);
    uint32_t line_num = 0;
    const DebugLoc &loc = I->getDebugLoc();
    if (loc) line_num = loc.getLine();
    Constant *LineNum = IRB.getInt32(line_num);
    IRB.CreateCall(TraceCallGraph, {CallerPtr, CalleePtr, LineNum});
  }

  void insert_pllr_call(BasicBlock *B, uint32_t idx, double weight) {
    Instruction *TI = B->getTerminator();
    BasicBlock *Succ = TI->getSuccessor(idx);
    BasicBlock *newBB = SplitEdge(B, Succ);
    if (!newBB) {
      errs() << "  [-] Failed to split edge\n";
      return;
    }
    BasicBlock::iterator IP = newBB->getFirstInsertionPt();
    IRBuilder<> IRB(&*IP);
    auto DoubleWeight = ConstantFP::get(DoubleTy, weight);
    ConstantInt *BlockId = getEdgeID(TI, 0);
    ConstantInt *EdgeId = getEdgeID(TI, idx);
    if (!EdgeId) {
      errs() << "  [-] Failed to get edge id for " << TI->getDebugLoc() << "\n";
      BlockId = ConstantInt::get(Int32Ty, cur_block_id);
      EdgeId  = ConstantInt::get(Int32Ty, cur_edge_id);
    }
#ifndef NDEBUG
    errs() << "  inserting call with edge_id " << format("%ld", EdgeId->getSExtValue()) << "\n";
#endif
    IRB.CreateCall(CalculatePathLLR, {DoubleWeight, BlockId, EdgeId})->setCannotMerge();
  }

  double getEdgeWeight(BasicBlock *BB, Instruction *TI, unsigned idx) {
    double weight = 0.0;
    uint32_t numerator = 1;
    uint64_t md_weight = 0;
    MDNode *WeightsNode =TI->getMetadata(LLVMContext::MD_prof);

    if (WeightsNode) {
      ConstantInt *Weight =
        mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(idx + 1));
        if (Weight)
          md_weight = Weight->getZExtValue();
    }

    auto BranchProb = curBPI->getEdgeProbability(BB, idx);
    // If UseHeuristic has been requested OR
    // if the metadata weight > 1 (profiling information available)
    if (md_weight > 1 || UseHeuristic) {
      numerator = BranchProb.getNumerator();
    }

    weight = ((double)numerator) / ((double)BranchProb.getDenominator());
    insert_pllr_call(BB, idx, weight);
    return weight;
  }
}; // end of class InstrumentPathLLR
} // end of llvm namespace

namespace {
  // Legacy PM implementation
struct PathLLR : public ModulePass {
  static char ID;
  PathLLR() : ModulePass(ID) {}

  bool runOnModule(Module &M) override {
    auto LookupBPI = [this](Function &F) {
      return &this->getAnalysis<BranchProbabilityInfoWrapperPass>(F).getBPI();
    };

    InstrumentPathLLR InstrumentPathLLR(&M, LookupBPI);

    return true;
  }
  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesAll();
    AU.addRequired<BranchProbabilityInfoWrapperPass>();
  }

}; // end of class PathLLR
}  // end of anonymous namespace

char PathLLR::ID  = 0;

// Register the pass
static RegisterPass<PathLLR>
  X(/*PassArg=*/"path-llr",
    /*Name=*/"Path log likelihood",
    /*CFGOnly=*/false,
    /*is_analysis*/false);

static void registerPathLLRPass(const PassManagerBuilder &,
                           legacy::PassManagerBase &PM) {
  PM.add(new PathLLR());
}

static RegisterStandardPasses Y(
  PassManagerBuilder::EP_FullLinkTimeOptimizationLast, registerPathLLRPass);

static RegisterStandardPasses Y0(
  PassManagerBuilder::EP_EnabledOnOptLevel0, registerPathLLRPass);
