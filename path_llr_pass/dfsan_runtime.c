#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "sanitizer/dfsan_interface.h"

/* DFSan tracking */
dfsan_label __pllr_dfsan_input_label = 1;
dfsan_label __pllr_dfsan_harness_label = 2;
#define KNOWN_LABEL_UNION 3
#define KNOWN_LABELS 2
const char *__pllr_dfsan_known_label_desc[] = {
  "input", "harness", "null"
};
uint64_t __pllr_dfsan_label_matched;

typedef struct dfsan_addr_label {
  void *addr;
  dfsan_label label;
} dfsan_addr_label;

struct dfsan_addr_label __pllr_last_cmp_label;
struct dfsan_addr_label __pllr_last_load_label;
struct dfsan_addr_label __pllr_last_store_label;

void __dfsan_load_callback(dfsan_label Label, void* addr) {
  __pllr_last_load_label.addr = addr;
  __pllr_last_load_label.label = Label;
}

void __dfsan_store_callback(dfsan_label Label, void*addr) {
  __pllr_last_store_label.addr = addr;
  __pllr_last_store_label.label = Label;
}

void __dfsan_mem_transfer_callback(dfsan_label *Start, size_t Len) {

}

void __dfsan_cmp_callback(dfsan_label CombinedLabel) {
  __pllr_dfsan_label_matched = 0;
  __pllr_last_cmp_label.addr = __builtin_return_address(0);
  __pllr_last_cmp_label.label = CombinedLabel;
  __pllr_dfsan_label_matched = CombinedLabel & KNOWN_LABEL_UNION;
}

void dfsan_set_label_harness(void *addr, size_t size) {

}

void dfsan_set_label_input(void *addr, size_t size) {

}

__attribute__((weak)) void 
dfsan_set_label(dfsan_label label, void *add, size_t size) {

}
