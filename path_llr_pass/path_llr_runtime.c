#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "sanitizer/dfsan_interface.h"

#include "khash.h"

#define MAX_EDGES 131072
#define LOGGING_MOD 10

double __pllr_cur_prob;
double __pllr_uniq_prob;
double __pllr_low_prob;
uint64_t __pllr_num_edges;
uint64_t __pllr_nuniq_edges;
uint32_t __pllr_verbose;
const char *__pllr_critical_edge_filename;
const char *__pllr_critical_block_filename;
uint8_t __pllr_edges[MAX_EDGES];

enum {
  UNIQ_EDGE = 1,
  CRITICAL_EDGE = 2,
  CRITICAL_BLOCK = 4
};

/* shared with path_llr pass */
extern uint64_t __pllr_total_edges;
extern uint64_t __pllr_num_functions;
extern uint64_t __pllr_function_addrs[];
extern char *__pllr_function_names[];

/* DFSan tracking */
enum {
  NULL_LABEL,
  INPUT_LABEL,
  HARNESS_LABEL,
  KNOWN_LABELS
};

dfsan_label __pllr_dfsan_input_label = INPUT_LABEL;
dfsan_label __pllr_dfsan_harness_label = HARNESS_LABEL;
#define KNOWN_LABEL_UNION 3
const char *__pllr_dfsan_known_label_desc[] = {
  "input", "harness", "null"
};
static __thread uint64_t __pllr_dfsan_label_matched;

typedef struct dfsan_addr_label {
  void *addr;
  dfsan_label label;
} dfsan_addr_label;

static __thread struct dfsan_addr_label __pllr_last_cmp_label;
static __thread struct dfsan_addr_label __pllr_last_load_label;
static __thread struct dfsan_addr_label __pllr_last_store_label;

// extern int LLVMFuzzerTestOneInput(const uint8_t*, size_t);
extern int LLVMFuzzerTestOneInput(const unsigned char *Data, size_t Size);


typedef union {
    struct { uint16_t src, dst, line, indirect; };
    uint64_t edge;
} cg_edge;

// Initialize structs and methods for callgraph hash table
// #define hash_func(key) (uint32_t)((key)>>33^(key)^(key)<<11)
// #define hash_eq(a, b) (a == b)
KHASH_INIT(cg, khint64_t, khint32_t, 1, kh_int64_hash_func, kh_int64_hash_equal)
khash_t(cg) *CG;


static inline uint32_t get_src(uint64_t src_dst) {
  return src_dst >> 32;
}

static inline uint32_t get_dst(uint64_t src_dst) {
  return src_dst & UINT32_MAX;
}


static int __pllr_read_critical_id_list(const char *filename, uint8_t type) {
  FILE *fp;
  char line[1024];
  int absent, id = 0, count = 0;
  khint_t it;

  fp = fopen(filename, "r");
  if (!fp) {
    fprintf(stderr, "[-] Error: failed to open '%s'\n", filename);
    return count;
  }

  while (fgets(line, 1023, fp)) {
    errno = 0;
    id = strtol(line, NULL, 10);
    if (errno != ERANGE) {
      __pllr_edges[id] |= type;
      count ++;
    }
  }
  fclose(fp);
  fprintf(stderr, "[+] read %d critical ids from %s.\n", count, filename);
  return count;
}


void __pllr_initialize(void) {
  char *verbose = getenv("PLLR_VERBOSE");

  if (verbose) {
    __pllr_verbose = strtol(verbose, NULL, 10);
    fprintf(stderr, "edges,path_llr,edge_id\n");
  }

  CG = kh_init(cg);

  __pllr_critical_block_filename = getenv("PLLR_BLOCKLIST");

  if (__pllr_critical_block_filename) {
    __pllr_read_critical_id_list(__pllr_critical_block_filename, CRITICAL_BLOCK);
  }

  __pllr_critical_edge_filename = getenv("PLLR_EDGELIST");

  if (__pllr_critical_edge_filename) {
    __pllr_read_critical_id_list(__pllr_critical_edge_filename, CRITICAL_EDGE);
  }
}

void __pllr_calculate_path_llr(double weight, uint32_t block_id, uint32_t edge_id) {

  uint8_t do_print = 0;
  double log_weight = log(weight * 2.0);
  __pllr_cur_prob += log_weight;
  __pllr_num_edges++;
  double cur_weight = __pllr_cur_prob / ((double)__pllr_num_edges);

  if (!(__pllr_edges[edge_id] & UNIQ_EDGE)) {
    __pllr_edges[edge_id] |= UNIQ_EDGE;
    __pllr_uniq_prob += log_weight;
    __pllr_nuniq_edges++;
  }

  if (__pllr_critical_block_filename && (__pllr_edges[block_id] & CRITICAL_BLOCK)) {
    do_print++;
  }

  if (__pllr_critical_edge_filename && (__pllr_edges[edge_id] & CRITICAL_EDGE)) {
    do_print++;
  }

  if (__pllr_verbose && __pllr_num_edges % LOGGING_MOD == 0) {
    do_print++;
  }

  if (!do_print)
    return;

  fprintf(stderr, "  pllr.CE, %04lu,%0.9f,%u,%u,%u\n",
          __pllr_num_edges, cur_weight, block_id, edge_id, __pllr_last_cmp_label.label);
  if (__pllr_dfsan_label_matched) {
    for (int i = 0; i < KNOWN_LABELS; i++) {
      if (1 << i & __pllr_dfsan_label_matched) {
        fprintf(stderr, "  \t CMP %s label from addr %p\n",
          __pllr_dfsan_known_label_desc[i], __pllr_last_cmp_label.addr);
      }
    }
  }
}

void __pllr_trace_call(uint64_t caller, uint64_t callee, uint32_t line) {
  cg_edge t = {0, 0, line, 0};
  int absent;

  for (int i = 0; i < __pllr_num_functions; i++) {
    if (__pllr_function_addrs[i] == caller) {
      t.src = i;
    }
    if (__pllr_function_addrs[i] == callee) {
      t.dst = i;
    }
  }

//uint64_t edge = ((uint64_t) t.src) << 32 | ((uint64_t) t.dst);
  khint_t it = kh_put(cg, CG, t.edge, &absent);

  if (!absent) kh_value(CG, it)++;
  else kh_value(CG, it) = 1;

  if (__pllr_verbose > 1) {
    fprintf(stderr, " 0x%08lx ==> 0x%08lx \t", caller, callee);
    fprintf(stderr, " caller(%d, %s) ", t.src, __pllr_function_names[t.src]);
    fprintf(stderr, " callee(%d, %s) ", t.dst, __pllr_function_names[t.dst]);
    fprintf(stderr, " absent=%d  count=%u\n", absent, kh_value(CG, it));
  }
}

static void __pllr_write_callgraph(void) {
  FILE *cg_log;
  khint_t itr;
  cg_edge t;
  uint32_t src_addr, dst_addr, count;
  char *src_name, *dst_name;

  khint_t hsize = kh_size(CG);

  char *logfilename = getenv("PLLR_CALLGRAPH_LOG");
  if (logfilename)
    cg_log = fopen(logfilename, "w");
  else
    cg_log = fopen("callgraph.log", "w");

  for(itr = kh_begin(CG); itr != kh_end(CG); ++itr) {
    if (!kh_exist(CG, itr)) continue;
    t.edge = kh_key(CG, itr);
/*
    t.src = get_src(kh_key(CG, itr));
    t.dst = get_dst(kh_key(CG, itr));
*/
    count = kh_value(CG, itr);
    src_addr = __pllr_function_addrs[t.src];
    dst_addr = __pllr_function_addrs[t.dst];
    src_name = __pllr_function_names[t.src];
    dst_name = __pllr_function_names[t.dst];
    fprintf(cg_log, "0x%x\t0x%x\t%s\t%s\t%d\t%d\n",
           src_addr, dst_addr,
           src_name, dst_name,
           t.line, count);
  }

  fclose(cg_log);
}

void __pllr_finalize(void) {

  double cur_weight = __pllr_cur_prob / ((double)__pllr_num_edges);
  double uniq_weight = __pllr_uniq_prob / ((double)__pllr_nuniq_edges);
  fprintf(stderr, "%04lu,%0.9f,%0.9f\n", __pllr_num_edges, cur_weight, uniq_weight);

  if (kh_size(CG)) {
    __pllr_write_callgraph();
  }
//  if (kh_size(CG)) kh_destroy(cg, CG);

}


void __dfsan_load_callback(dfsan_label Label, void* addr) {
  __pllr_last_load_label.addr = addr;
  __pllr_last_load_label.label = Label;
}

void __dfsan_store_callback(dfsan_label Label, void*addr) {
  __pllr_last_store_label.addr = addr;
  __pllr_last_store_label.label = Label;
}

void __dfsan_mem_transfer_callback(dfsan_label *Start, size_t Len) {

}

void __dfsan_cmp_callback(dfsan_label CombinedLabel) {
  __pllr_dfsan_label_matched = 0;
  __pllr_last_cmp_label.addr = __builtin_return_address(0);
  __pllr_last_cmp_label.label = CombinedLabel;
  __pllr_dfsan_label_matched = CombinedLabel & KNOWN_LABEL_UNION;
}

__attribute__((weak)) void
dfsan_set_label(dfsan_label label, void *add, size_t size) {
}

__attribute__((visibility("default"))) int
__dfsw_LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
    dfsan_set_label(__pllr_dfsan_input_label, &size, sizeof(size));
    dfsan_set_label(__pllr_dfsan_input_label, (void*)data, size);
    fprintf(stderr, "  Set data and size to input label.\n");

  return LLVMFuzzerTestOneInput(data, size);
}

__attribute__((visibility("default")))
void dfsan_set_label_harness(void *addr, size_t size) {
  dfsan_set_label(HARNESS_LABEL, addr, size);
}

__attribute__((visibility("default")))
void dfsan_set_label_input(void *addr, size_t size) {
  dfsan_set_label(INPUT_LABEL, addr, size);
}

