

# Path Log Likelihood Ratio (LLR)


```math
{\sf normalized\_path\_llr}(Path_{i}) = \cfrac{\sum_{edge_{ij}} \ln{\frac{P(edge_{ij})}{0.5}}}{len(Path_i)}
```

or

```math
{\sf normalized\_path\_llr}(Path_{i}) = \cfrac{\sum_{edge_{ij}} \ln\big(2.0 * P(edge_{ij})\big)}{len(Path_i)}
```


## Quickstart

The LLVM-12 / AFL++ docker container has all dependencies installed.

`$ docker pull registry.gitlab.com/fuzzing/directed-fuzzing/llvm-12:16.04`

For convenience: 

`$ docker tag registry.gitlab.com/fuzzing/directed-fuzzing/llvm-12:16.04 pathllr`

The `profile_fuzz.sh` script defaults to fuzzing in `/dev/shm/fuzz`, so mount that directory
and the current (source) directory in the running container.

`$ docker run -it --rm --name dkr-fuzz-pathllr -v /dev/shm/fuzz:/dev/shm/fuzz -v $(pwd):$(pwd) -w $(pwd) pathllr`

Build the LLVM pass libraries and runtime.

`$ cmake -B build_pass -G Ninja -DCMAKE_BUILD_TYPE=DebWithRelInfo`

`$ ninja -C build_pass`

The build scripts automates most of the steps.

`$ ./scripts/build.sh` [build.sh](../build.sh) 


```
./scripts/build.sh <command>

  download   1) download source for benchmarks
  coverage   2) compile coverage binaries
  aflpp      3) compile with AFL++ instrumentation
  install    4) install coverage and AFL++ binaries in 'bin' 
             5a) ./scripts/profile_fuzz.sh to generate profraw files
             5b) post process profraw with llvm-profdata to profdata files.
  pgo        6) run LLVM pass to generate bitcode with branch probabilities
  pllr       7) run LLVM pass to compile a final binary that calculates
                Path log-likelihood during runtime.
  dump-ce    8) run LLVM pass to dump critical edges based on PGO data.
  dfsan      9) run LLVM pass to compile DFSan binary that logs
                critical edge data 
  find      10) ./scripts/find_solutions.py to execute DFSan binary
                on a queue of inputs

Optional commands:
  check-profdata  check profdata files for execution counts
  html            generate llvm-cov html reports
  mcmc            do MCMC random walks 
```

### Fuzzing 

`./scripts/profile_fuzz.sh` will launch a fuzzing session for a specified duraction on one benchmark.
With no arguments, the defaults are used:
- benchmark: freetype2
- hours of fuzzing: 48
- initial seeds directory: `seeds`
- do not add solution inputs (used in evalution).

The command line options are:

`./scripts/profile_fuzz.sh <benchmark_folder> <# of hours to fuzz> <initial seeds directory name>`

The script will invoke `./scripts/afl-system-config` which alters system settings for fuzzing.
This script was copied from the AFLplusplus repository.
You may wish to alter this script if you don't want system security settings changed.

Fuzzing will be conducted in `/dev/shm/fuzz/profile/...`.



You can also download compiled binaries from CI jobs 
[artifacts.zip](https://gitlab.com/fuzzing/directed-fuzzing/-/jobs/artifacts/master/download?job=install).
