# PathLLR Crtical Edges

### checker ( using json seed, added indirect call edges )
```
readelf: %22-->%28              0.000001934  574  16   558  4
readelfphdrs: %2-->%14          0.000000000  193  26   167  6
readelf32phdr: %2-->%17         0.000000000  142  15   127  4
readelf64phdr: %2-->%17         0.000000000  142  15   127  4
json_parse_ex: %1627-->%1631    0.000000000  135  4    131  0
json_parse_ex: %638-->%644      0.000000000  130  126  4    25
unpackelf32shdr: %4-->%14       0.000000000  130  120  10   0
unpackelf64shdr: %4-->%14       0.000000000  130  120  10   0
unpackelf32phdr: %4-->%14       0.000000000  106  98   8    0
unpackelf64phdr: %4-->%14       0.000000000  106  98   8    0
```


### file

```
Function      Edge              Probability  Weight I     C     B
apprentice_1: %3-->%15          0.666666667  10066  107   9959  24
file_trycdf: %2-->%41           0.666666667  3785   92    3693  28
file_trycdf: %119-->%122        0.000000000  2593   16    2577  4
mget: %137-->%144               0.001508296  1832   1674  158   156
file_buffer: %6-->%36           0.666666667  1422   272   1150  72
file_zmagic: %89-->%93          0.000000000  1054   71    983   10
file_fsmagic: %41-->%65         0.666666667  882    422   460   106
file_tryelf: %118-->%124        0.250000000  657    252   405   27
file_tryelf: %118-->%308        0.500000000  653    248   405   27
do_os_note: %9-->%60            0.666666667  524    212   312   57
json_parse: %4-->%21            0.666666667  443    57    386   11
check_buffer: %43-->%50         0.666666667  370    109   261   15
magiccheck: %2-->%439           0.001412429  340    21    319   4
file_or_fd: %3-->%22            0.666666667  316    181   135   48
file_fmtcheck: %5-->%18         0.111111111  298    18    280   2
file_getbuffer: %1-->%20        0.666666667  266    261   5     26
file_encoding: %7-->%32         0.333333333  252    197   55    30
file_tryelf: %2-->%37           0.666666667  232    91    141   18
file_strncmp: %4-->%45          0.070754717  224    214   10    38
match: %245-->%249              0.007042253  216    216   0     40
```


### freetype2

```
pfr_face_init: %38-->%46                           0.000000000  2008  169  1839  29
sfnt_open_font: %32-->%35                          0.000000000  1097  27   1070  6
af_loader_load_glyph: %183-->%187                  0.000000000  1065  25   1040  3
tt_hvadvance_adjust: %4-->%20                      0.000000000  942   80   862   11
_ZL19setIntermediateAxisP11FT_FaceRec_: %1-->%17   0.000000001  921   77   844   12
pcf_load_font: %72-->%79                           0.000001356  771   74   697   15
cf2_interpT2CharString: %1471-->%1479              0.000000643  766   766  0     163
tt_face_init: %219-->%227                          0.000000001  744   14   730   3
tt_done_blend: %1-->%16                            0.000000000  673   241  432   39
cid_face_open: %150-->%155                         0.000000000  659   160  499   39
cf2_glyphpath_pushPrevElem: %37-->%53              0.000000000  553   15   538   2
cf2_interpT2CharString: %1148-->%1151              0.000000000  519   179  340   9
IsMacResource: %30-->%38                           0.000000039  487   26   461   4
load_truetype_glyph: %612-->%633                   0.000000003  456   456  0     20
TT_Load_Glyph: %18-->%23                           0.000000000  396   233  163   22
tt_face_load_sbit: %80-->%84                       0.000000001  367   367  0     47
cid_face_open: %39-->%51                           0.000000080  356   107  249   13
fnt_face_get_dll_font: %279-->%284                 0.000002311  337   337  0     71
load_mac_face: %37-->%43                           0.000000000  322   8    314   0
```


### lcms

```
cmsCloseProfile: %14-->%19                     0.000000000  1157  14   1143  0
OptimizeByComputingLinearization: %46-->%53    0.000001014  1043  491  552   97
_cmsHandleExtraChannels: %6-->%36              0.000000044  1028  116  912   17
ComputeConversion: %7-->%27                    0.000000023  878   38   840   2
OptimizeByJoiningCurves: %39-->%51             0.000001014  613   209  404   34
OptimizeMatrixShaper: %106-->%120              0.000000000  410   64   346   10
OptimizeByResampling: %39-->%52                0.000001014  384   182  202   41
cmsDetectDestinationBlackPoint: %327-->%328    0.000000000  341   85   256   10
cmsCreateExtendedTransform: %121-->%155        0.000000044  294   16   278   1
cmsCreateExtendedTransform: %238-->%242        0.000000044  210   8    202   0
_MultiplyMatrix: %45-->%58                     0.000001166  155   68   87    9
cmsDetectDestinationBlackPoint: %125-->%132    0.000000000  154   154  0     29
_cmsReadInputLUT: %125-->%129                  0.000000003  122   4    118   0
AllocEmptyTransform: %53-->%56                 0.000001014  111   106  5     5
_cmsReadOutputLUT: %15-->%28                   0.000000046  110   5    105   0
_cmsReadOutputLUT: %100-->%104                 0.000000046  102   4    98    0
cmsDetectBlackPoint: %69-->%72                 0.000000156  101   13   88    2
OptimizeMatrixShaper: %5-->%31                 0.000001014  91    91   0     16
cmsReverseToneCurveEx: %90-->%108              0.000000000  67    67   0     5
AddMLUBlock: %17-->%25                         0.000000000  65    7    58    2
```


### libjpeg-turbo

```
start_pass.193: %84-->%311                0.000000000  682  682  0    0
jinit_master_decompress: %121-->%125      0.000000000  573  5    568  0
jinit_master_decompress: %121-->%128      0.000000000  445  13   432  2
decode_mcu.202: %411-->%777               0.000000000  405  405  0    44
realize_virt_arrays: %276-->%290          1.000000000  296  291  5    17
jinit_color_deconverter: %53-->%56        0.000000000  252  199  53   18
jinit_color_deconverter: %53-->%137       0.000000000  203  56   147  12
read_markers: %1174-->%1181               0.000000000  184  176  8    0
jinit_master_decompress: %143-->%146      0.000000000  157  2    155  0
consume_markers: %425-->%480              0.000000001  149  149  0    2
jpeg_calc_output_dimensions: %121-->%136  0.000000000  122  122  0    14
request_virt_barray: %6-->%69             0.000000000  100  96   4    10
use_merged_upsample: %1-->%5              0.000000000  96   96   0    15
jinit_color_deconverter: %53-->%179       0.000000000  94   21   73   3
jinit_d_main_controller: %140-->%167      0.000000001  89   87   2    2
jinit_d_coef_controller: %115-->%145      0.000000001  87   85   2    2
jpeg_calc_output_dimensions: %436-->%468  0.000000001  87   87   0    2
tjDecompress2: %149-->%160                0.000000000  86   86   0    8
realize_virt_arrays: %1-->%8              0.000000000  84   83   1    7
null_convert: %5-->%10                    0.000000000  79   79   0    7
```


### libpcap

```
swap_pseudo_headers: %3-->%11          0.000000275  714  4    710  0
swap_pseudo_headers: %3-->%14          0.000000275  714  4    710  0
pcap_parse: %230-->%1306               0.000000000  559  12   547  2
pcap_parse: %230-->%1317               0.000000000  555  8    547  2
pcap_parse: %230-->%1245               0.000000000  460  12   448  2
pcap_parse: %230-->%1256               0.000000000  456  8    448  2
yy_get_next_buffer: %54-->%80          0.000000000  289  281  8    36
pcap_parse: %230-->%1168               0.000000000  281  12   269  2
pcap_parse: %230-->%991                0.000000000  267  17   250  2
gen_scode: %40-->%389                  0.000000013  234  132  102  39
gen_scode: %40-->%505                  0.000000013  200  19   181  2
gen_scode: %40-->%521                  0.000000013  199  18   181  2
gen_ecode: %3-->%17                    0.000000637  183  182  1    23
swap_pseudo_headers: %3-->%17          0.000000275  157  4    153  0
pcap_parse: %230-->%1220               0.000000000  142  8    134  2
pcap_parse: %230-->%1227               0.000000000  142  8    134  2
pcap_lex: %817-->%837                  0.000000000  139  43   96   2
pcap_parse: %230-->%1288               0.000000000  139  12   127  2
pcap_parse: %230-->%1157               0.000000000  137  12   125  2
pcap_parse: %230-->%1299               0.000000000  135  8    127  2
```


### libxml2

```
xmlStringLenDecodeEntities: %388-->%391              0.000000000  1234  150  1084  26
xmlSAX2GetEntity: %99-->%109                         0.000000008  819   80   739   10
xmlParseReference: %284-->%304                       0.000000000  771   39   732   2
xmlParseReference: %579-->%599                       0.000000000  771   39   732   2
xmlParseElement: %87-->%100                          0.000000000  732   4    728   0
xmlSAX2ExternalSubset: %29-->%39                     0.000000066  655   259  396   20
xmlParseReference: %284-->%289                       0.000000000  619   18   601   0
xmlParseReference: %579-->%584                       0.000000000  619   18   601   0
xmlSAX2AttributeDecl: %110-->%115                    0.000000006  481   25   456   2
xmlSAX2ElementDecl: %65-->%70                        0.000000020  367   30   337   3
__xmlIOErr: %3-->%10                                 0.000000059  366   314  52    104
xmlParsePI: %562-->%566                              0.000000017  318   13   305   2
areBlanks: %4-->%25                                  0.000000005  300   199  101   41
xmlParseReference: %559-->%675                       0.000000000  284   246  38    38
xmlParseElement: %504-->%522                         0.000000001  268   4    264   0
xmlParsePEReference: %77-->%121                      0.000000015  258   258  0     34
xmlSAX2AttributeNs: %276-->%281                      0.000000007  247   186  61    20
xmlSaveUri: %1408-->%1413                            0.000000014  234   234  0     38
xmlSaveUri: %1177-->%1182                            0.000000017  234   234  0     38
xmlSaveUri: %93-->%98                                0.000000014  224   224  0     35
```


### proj4

```
pj_projection_specific_setup_lsat: %91-->%112       0.000000000  1016  180  836  8
pj_projection_specific_setup_pipeline: %160-->%167  0.000000005  624   217  407  31
pj_gridinfo_init: %111-->%114                       0.000000024  619   15   604  2
read_vgrid_value: %50-->%51                         0.000000000  510   449  61   40
get_init: %27-->%36                                 0.000000000  356   37   319  6
pj_gridinfo_init: %154-->%157                       0.000000024  283   81   202  3
isea_forward: %2-->%24                              0.000000008  274   76   198  7
pj_gridinfo_init: %133-->%149                       0.000000054  273   5    268  0
pj_gridinfo_init: %85-->%91                         0.000000024  269   20   249  3
pj_apply_gridshift_2: %30-->%70                     0.000000003  233   17   216  0
pj_projection_specific_setup_lcc: %90-->%104        0.000000027  233   214  19   10
s_inverse.1620: %60-->%64                           0.000000000  185   185  0    18
pj_projection_specific_setup_pipeline: %243-->%257  0.000000000  157   47   110  5
geod_genposition: %500-->%504                       0.000000094  135   135  0    4
get_init_string: %85-->%101                         0.000000040  134   125  9    20
geographic_to_cartesian: %7-->%27                   0.000000000  109   109  0    16
pj_projection_specific_setup_omerc: %260-->%270     0.000000000  97    92   5    4
geod_geninverse_int: %689-->%693                    0.000000027  96    95   1    3
pj_projection_specific_setup_laea: %91-->%117       0.000000003  92    87   5    0
setup.841: %32-->%167                               0.000000014  92    86   6    9
```
