


## Indirect call analysis

### MetaCG
- paper "MetaCG: annotated call-graphs to facilitate whole-program analysis" TAPAS '20 [paper](https://dl.acm.org/doi/abs/10.1145/3427764.3428320)
- MetaCG repo (https://github.com/tudasc/MetaCG)

### PHASAR
- 
- LLVM-based static analysis framework
- site https://phasar.org/ (from https://www.hni.uni-paderborn.de/sse/ )
- GH repo ( https://github.com/secure-software-engineering/phasar )


## Analysis of indirect calls found during fuzzing

| Benchmark | ind-call-sites | discovered sites | discovered edges | 
| :-------: | :------------: | :--------------: | :--------------: |
| file      | 3              | 0                | 0          |
| freetype2 | 654            | 336              | 583        |
| lcms      | 594            | 123              | 189        |
| libjpeg-t | 603            | 254              | 287        |
| libpcap   | 14             | 11               | 15         |
| libxml2   | 1027           | 310              | 336        |
| proj4     | 495            | 84               | 264        |

## Presense of indirect calls in programs


```
 ==== benchmarks/checker/pgo-checker-v0/fuzzer.bc ===
  calls          446
  direct calls   374
  indirect calls 72
  invoke calls   0
 ==== benchmarks/file/pgo-file-FILE5_38/fuzzer.bc ===
  calls          3605
  direct calls   3602
  indirect calls 3
  invoke calls   8
 ==== benchmarks/freetype2/pgo-freetype-2.9/fuzzer.bc ===
  calls          16992
  direct calls   16338
  indirect calls 654
  invoke calls   68
 ==== benchmarks/lcms/pgo-lcms2.10/fuzzer.bc ===
  calls          7644
  direct calls   7050
  indirect calls 594
  invoke calls   0
 ==== benchmarks/libjpeg-turbo/pgo-libjpeg-turbo-2.0.6/fuzzer.bc ===
  calls          13954
  direct calls   13351
  indirect calls 603
  invoke calls   3
 ==== benchmarks/libpcap/pgo-libpcap-1.10.0/fuzzer.bc ===
  calls          3892
  direct calls   3878
  indirect calls 14
  invoke calls   0
 ==== benchmarks/libxml2/pgo-libxml2-v2.9.8/fuzzer.bc ===
  calls          9321
  direct calls   8294
  indirect calls 1027
  invoke calls   0
 ==== benchmarks/proj4/pgo-PROJ-5.2.0/fuzzer.bc ===
  calls          10444
  direct calls   9949
  indirect calls 495
  invoke calls   0
```
