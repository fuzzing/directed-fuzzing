#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

#include "llvm/ADT/GraphTraits.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/ADT/SCCIterator.h"

#include "llvm/Support/Casting.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/BranchProbability.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Analysis/BlockFrequencyInfo.h"
#include "llvm/Analysis/BranchProbabilityInfo.h"

#include <iostream>

using namespace llvm;

#define DEBUG_TYPE "dump-cfg"
#define MIN_FUNC_WAS_EXECUTED 50000
#define MIN_BLK_PROFILE_CNT 50000
#define MIN_EDGE_PROFILE_CNT 50
#define MIN_EDGE_WEIGHT 20

static cl::opt<std::string>
CallGraphFile("icfg", cl::Hidden,
              cl::desc("Supplemental/ dynamic callgraph edges logfile."));

static cl::opt<std::string>
EdgeWeightsLogfile("ce-out",
                   cl::init("edge_weights.csv"),
                   cl::desc("Edge weights output filename"),
                   cl::value_desc("filename"));


static cl::opt<std::string>
UnreachableFuncsLogFile("ur-out",
                       cl::init("unreachable_funcs.csv"),
                       cl::desc("Unreachable functions output filename"),
                       cl::value_desc("filename"));

static cl::opt<bool>
DumpAllEdges("all-edges", cl::init(false), cl::Hidden,
             cl::desc("Dump all edges, not just critical ones."));

using DomTreeCallback = function_ref<const DominatorTree *(Function &F)>;
using PostDomTreeCallback = function_ref<const PostDominatorTree *(Function &F)>;
using BranchProbabilityInfoCallback = function_ref<const BranchProbabilityInfo *(Function &F)>;
using BlockFrequencyInfoCallback = function_ref<const BlockFrequencyInfo *(Function &F)>;

typedef SmallPtrSet<Function *, 4> FunctionSet;

struct GraphEdge {
  const Value *From;
  uint32_t idx;
};

struct EdgeWeight {
    double prob;
    uint64_t epc;
    uint32_t nDom;
    uint32_t calls;
    uint32_t iSum;
};

struct BlockCounts {
    uint32_t bSum;
    uint32_t iSum;
    uint32_t calls;
    uint32_t __count_calls;
    uint64_t bfi;
    uint64_t bpc;
};

struct CallEdge {
  Function *callee;
  Function *caller;
  uint32_t line;
};

bool operator==(GraphEdge LHS, GraphEdge RHS) {
  return LHS.From == RHS.From && LHS.idx == RHS.idx;
}

bool operator<(GraphEdge LHS, GraphEdge RHS) {
  return std::less<const Value *>()(LHS.From, RHS.From) ||
  (LHS.From == RHS.From && LHS.idx < RHS.idx);
}

namespace llvm {

// Specialize DenseMapInfo For GraphEdge
template <> struct DenseMapInfo<GraphEdge> {
  static GraphEdge getEmptyKey() {
    return GraphEdge{DenseMapInfo<Value *>::getEmptyKey(),
    DenseMapInfo<uint8_t>::getEmptyKey()};
  }

  static GraphEdge getTombstoneKey() {
    return GraphEdge{DenseMapInfo<Value *>::getTombstoneKey(),
    DenseMapInfo<uint8_t>::getEmptyKey()};
  }

  static unsigned getHashValue(const GraphEdge  &Edge) {
    return DenseMapInfo<std::pair<const Value *, uint8_t>>::getHashValue(
      std::make_pair(Edge.From, Edge.idx));
  }

  static bool isEqual(const GraphEdge &LHS, const GraphEdge &RHS) {
    return LHS== RHS;
  }
};
}  // end of namespace llvm

static bool isUniqCall(Function &calledFunction, bool verbose);


static std::string getNodeLabel(const BasicBlock *Node, bool verbose = true) {
  std::string FuncName = "";
  if (verbose) {
    FuncName = Node->getParent()->getName().str() + ": ";
  }

  if (!Node->getName().empty())
    return FuncName + Node->getName().str();

  std::string Str;
  raw_string_ostream OS(Str);

  Node->printAsOperand(OS, false);
  return FuncName + OS.str();
}

static std::string getFuncLabel(const Function *F) {
  std::string Str;
  raw_string_ostream OS(Str);
  OS << F->getName() << format( "(ec=%d)", F->getEntryCount().getCount());
  return OS.str();
}

static std::string getCGNodeLabel(const CallGraphNode *Node) {
  std::string Str;
  raw_string_ostream OS(Str);
  Node->print(OS);
  return OS.str();
}

static void dumpBlockInfo(BasicBlock *BB, struct BlockCounts *bc) {
  if (bc->iSum + bc->calls > 0) {
    errs() << "   " << getNodeLabel(BB);
    errs() << format("\ti=%4u  b=%4u calls=%3u, bfi=%lu, bpc=%lu\n", bc->iSum, bc->bSum, bc->calls, bc->bfi, bc->bpc);
  }
}

static void dumpFunctionInfo(Function *F) {
  errs() << "  " << F->getName() << "\n";
  errs() << "     hasProfileData = " << F->hasProfileData() << "\n";
  errs() << "     entry counts   = " << F->getEntryCount().getCount() << "\n";
  errs() << "     isUniqueCall   = " << isUniqCall(*F, true) << "\n";
}

// True if block has successors and it dominates all of them.
static bool isFullDominator(const BasicBlock *BB, const DominatorTree *DT) {
  if (succ_empty(BB))
    return false;

  return llvm::all_of(successors(BB), [&](const BasicBlock *SUCC) {
    return DT->dominates(BB, SUCC);
  });
}

static bool checkRoots(const BasicBlock *BB, SmallPtrSetImpl<const BasicBlock*>&roots,
                       const DominatorTree *DT) {
  for (auto root : roots) {
    if (DT->dominates(root, BB))
      return true;
  }
  return false;
}

static bool wasExecuted(Function *F) {
  if (isa_and_nonnull<Function>(F)) {
    return F->getEntryCount().getCount() > MIN_FUNC_WAS_EXECUTED;
  }
  return false;
}

static bool wasExecuted(const Function *F) {
  if (isa_and_nonnull<Function>(F)) {
    return F->getEntryCount().getCount() > MIN_FUNC_WAS_EXECUTED;
  }
  return false;
}

static uint32_t numCallees(Function &calledFunction, bool verbose = false) {
  uint32_t counter = 0;
  verbose = verbose && !calledFunction.isDeclaration();
  SmallPtrSet<Function *, 16> Callees;
  if (verbose) {
    errs() << "<< ";
  }
  for (User *U : calledFunction.users()) {
    if (auto CI = dyn_cast<CallInst>(U)) {
      counter++;
      if (Callees.insert(CI->getFunction()).second && verbose)
        errs() << CI->getFunction()->getName() << ", ";
    }
  }
  return Callees.size();
}

static bool isUniqCall(Function &calledFunction, bool verbose = false) {
  return numCallees(calledFunction, verbose) <= 1;
}

static bool hasMetadata(const BasicBlock *BB, const Instruction *TI) {
  uint64_t weight = 1, md_weight = 0;
  MDNode *WeightsNode =TI->getMetadata(LLVMContext::MD_prof);
  if (!WeightsNode)
    return false;

  for (unsigned I = 0, E = TI->getNumSuccessors(); I < E; I++) {
    ConstantInt *Weight =
      mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(I + 1));
      if (Weight)
        md_weight = Weight->getZExtValue();
      if (md_weight > weight)
        weight = md_weight;
  }
  return weight > 1;
}

static void getIDs(Instruction *TI, unsigned idx, uint64_t *block_id, uint64_t *edge_id) {
  MDNode *WeightsNode = TI->getMetadata("pllr.edges");

  if (isa_and_nonnull<MDNode>(WeightsNode) & (WeightsNode->getNumOperands() > idx+1) & (TI->getNumOperands() > 1)) {
    ConstantInt *Weight =
      mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(idx + 1));
    *edge_id = Weight->getZExtValue();
    Weight = mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(1));
    *block_id = Weight->getZExtValue();
  }
  return;
}


class ModuleDumpCFG {
  using MapType = DenseMap<GraphEdge, EdgeWeight>;

public:
  ModuleDumpCFG( )  {}

  bool dumpModule(Module &M,
                  DomTreeCallback DTCallBack,
                  PostDomTreeCallback PDTCallback,
                  BranchProbabilityInfoCallback BPICallback,
                  BlockFrequencyInfoCallback BFICallback);
  void getFunctionWeights(Module &M);
  uint32_t getBlockWeights(const BasicBlock *BB, const DominatorTree *DT);
  void getEdgeWeights(Function &F, const DominatorTree *DT);
  void addEdge(const BasicBlock *BB, const BasicBlock *Succ, uint32_t idx);
  double getEdgeWeight(const BasicBlock *BB, unsigned idx);
  uint32_t countCalls(const BasicBlock &BB, SmallPtrSetImpl<Function *> &directCalls);
  uint32_t getDirectCallWeight(Function *F);
  uint32_t getIndirectCallWeight(const Instruction *I);
  uint64_t getFreq(const BasicBlock *BB);
  uint64_t getProfileCount(const BasicBlock *BB);
  void dumpEdgeWeights(Module &M);
  void addUnreachableFunctions(void);
  void dumpUnreachableFunctions(Module &M);
  void readCallGraphFile(Module &M);
  void DFS(Function *F, SmallPtrSetImpl<Function *> &worklist,
           SmallPtrSetImpl<Function *> &visited);
  FunctionSet getIndirectCallees(const Instruction *);

private:
  LLVMContext *C;
  const BranchProbabilityInfo *curBPI;
  const BlockFrequencyInfo *curBFI;
  const DominatorTree *curDT;
  MapType EdgeWeights;
  DenseMap<const BasicBlock *, BlockCounts> BlockDominators;
  DenseMap<const Function*, uint64_t> FunctionDominators;
  DenseMap<GraphEdge, FunctionSet> CallGraphEdges;
  SmallPtrSet<Function *, 32> UnreachableFunctions;
};


bool ModuleDumpCFG::dumpModule(
    Module &M, DomTreeCallback DTCallBack, PostDomTreeCallback PDTCallback,
    BranchProbabilityInfoCallback BPICallback, BlockFrequencyInfoCallback BFICallback) {

  C = &(M.getContext());
  errs() << "[*] Dumping " << M.getName() << "\n";

  if (!CallGraphFile.empty()) {
    errs() << "[*] Loading edges from " << CallGraphFile << "\n";
    readCallGraphFile(M);
  }

  getFunctionWeights(M);

  for (auto &F : M) {
    if (F.isDeclaration())
      continue;

    const DominatorTree *DT = DTCallBack(F);
    curBPI = BPICallback(F);
    curBFI = BFICallback(F);

    errs() << "  Basic blocks of " << getFuncLabel(&F) << " in post order:\n";
    for (auto BB : post_order(&F.getEntryBlock())) {
        getBlockWeights(BB, DT);
        dumpBlockInfo(BB, &BlockDominators[BB]);
    }
    if (wasExecuted(&F)) {
      getEdgeWeights(F, DT);
    }
  }
  addUnreachableFunctions();
  dumpEdgeWeights(M);
  dumpUnreachableFunctions(M);
  return false;
}

void ModuleDumpCFG::getFunctionWeights(Module &M) {
  SmallPtrSet<Function *, 32> worklist;
  for (auto &F : M) {
    worklist.insert(&F);
    // unique call means only one function calls this function
    if (isUniqCall(F, false)) {
      FunctionDominators[&F] = F.getInstructionCount();
    } else {
      FunctionDominators[&F] = 0;
    }
    if (!wasExecuted(&F) & !F.isDeclaration() & (numCallees(F) == 0)) {
      UnreachableFunctions.insert(&F);
    }
  }

  SmallPtrSet<Function *, 32> visited;
  for (auto &F : M) {
    DFS(&F, worklist, visited);
  }
}

uint32_t ModuleDumpCFG::getBlockWeights(const BasicBlock *BB, const DominatorTree *DT) {
  BlockDominators[BB] = {0, 0, 0, 0, 0};
  uint32_t count = 0;
  const Instruction *TI = BB->getTerminator();
//const BasicBlock *BBSucc;

  BlockDominators[BB].bfi = getFreq(BB);
  BlockDominators[BB].bpc = getProfileCount(BB);

  if (BlockDominators[BB].bpc < MIN_BLK_PROFILE_CNT) {
    SmallPtrSet<Function *, 8> directCalls;
    BlockDominators[BB].__count_calls = countCalls(*BB, directCalls);
    BlockDominators[BB].calls = BlockDominators[BB].__count_calls;
    BlockDominators[BB].iSum = BB->sizeWithoutDebug();
    SmallVector<BasicBlock *> Descendents;
    /* getDescendants has no const form */
    DT->getDescendants(const_cast<BasicBlock *>(BB), Descendents);
    for (const auto *BBSucc : Descendents) {
      if (BB != BBSucc){
        BlockDominators[BB].bSum += 1;
        BlockDominators[BB].iSum += BBSucc->sizeWithoutDebug();
//      BlockDominators[BB].calls += BlockDominators[BBSucc].__count_calls;
        BlockDominators[BB].calls += countCalls(*BBSucc, directCalls);
      }
    }
    for (Function *Callee : directCalls) {
      BlockDominators[BB].calls += getDirectCallWeight(Callee);
    }
  }
  return count;
}

void ModuleDumpCFG::getEdgeWeights(Function &F, const DominatorTree *DT) {
  const BasicBlock *BBSucc;
  /* roots are the leading edges of a dominance sub-tree
   * covered ensures only the leading edge is counted */
  SmallPtrSet<const BasicBlock *, 8> roots;
  SmallPtrSet<const BasicBlock *, 16> covered;
  ReversePostOrderTraversal<const Function *> RPOT(&F);
  for (const auto *BB : RPOT) {
    for(const_succ_iterator I = succ_begin(BB), E = succ_end(BB); I != E; ++I) {
      BBSucc = *I;
      if (DumpAllEdges) {
        addEdge(BB, BBSucc, I.getSuccessorIndex());
        continue;
      }
      if (BlockDominators[BBSucc].bpc > MIN_EDGE_PROFILE_CNT) {
        continue;
      }
      if (DT->dominates(BB, BBSucc)) {
        if (covered.contains(BB)) {
          covered.insert(BBSucc);
        } else if (checkRoots(BBSucc, roots, DT)) {
          covered.insert(BB);
          covered.insert(BBSucc);
        } else {
          addEdge(BB, BBSucc, I.getSuccessorIndex());
          covered.insert(BBSucc);
          roots.insert(BBSucc);
        }
      }
    }
  }
}

void ModuleDumpCFG::addEdge(const BasicBlock *BB, const BasicBlock *Succ, uint32_t idx) {
  struct GraphEdge edge;
  edge.From = BB;
  edge.idx = idx;
  EdgeWeights[edge].nDom = BlockDominators[Succ].bSum;
  EdgeWeights[edge].prob = getEdgeWeight(BB, idx);
  EdgeWeights[edge].calls = BlockDominators[Succ].calls;
  EdgeWeights[edge].iSum = BlockDominators[Succ].iSum;
  EdgeWeights[edge].epc = BlockDominators[BB].bpc;
}

void ModuleDumpCFG::addUnreachableFunctions(void) {

  for (Function *F : UnreachableFunctions) {
    const BasicBlock *B = &(F->getEntryBlock());
    struct GraphEdge edge;
    edge.From = B;
    edge.idx = 0;
    EdgeWeights[edge].nDom = BlockDominators[B].bSum;
    EdgeWeights[edge].prob = 0.0;
    EdgeWeights[edge].calls = BlockDominators[B].calls;
    EdgeWeights[edge].iSum = BlockDominators[B].iSum;
    EdgeWeights[edge].epc = 1;
  }
  errs() << "[*] Added unreachable functions to edge list.\n";
}

void ModuleDumpCFG::dumpUnreachableFunctions(Module &M) {
  std::error_code EC;
  raw_fd_ostream OS(UnreachableFuncsLogFile, EC, sys::fs::OF_None);

  for (Function *F : UnreachableFunctions) {
    OS << F->getName() << ",";
    OS << format("%u,", FunctionDominators[F]);
  }
  OS << "\n";
  OS.close();
}

void ModuleDumpCFG::dumpEdgeWeights(Module &M) {

  std::error_code EC;
  raw_fd_ostream OS(EdgeWeightsLogfile, EC, sys::fs::OF_None);
  uint64_t block_id = 0, edge_id = 0;

  for (auto it = EdgeWeights.begin(), end = EdgeWeights.end();
       it != end; it++) {
    GraphEdge e = it->first;
    EdgeWeight w = it->second;
    if (w.iSum + w.calls < MIN_EDGE_WEIGHT) continue;
    BasicBlock *BB = (BasicBlock*) e.From;
    Instruction *TI = BB->getTerminator();
    Instruction *srcI = BB->getFirstNonPHIOrDbg(true);
    getIDs(TI, e.idx, &block_id, &edge_id);
    OS << format("%03u, %03u, ", block_id, edge_id);
    OS << getNodeLabel(BB) << "-->";
    if (TI->getNumSuccessors() > 1) {
      BasicBlock *dst = TI->getSuccessor(e.idx);
      OS << getNodeLabel(dst, false);
      OS << ",";
      TI->getDebugLoc().print(OS);
      OS << ",";
      Instruction *dstI = dst->getFirstNonPHI();
      dstI->getDebugLoc().print(OS);
    } else {
      OS << "none,";
      TI->getDebugLoc().print(OS);
      OS << ",none";
    }
    OS << format(",%0.9f,%u,%u,%u,%u,%llu\n",
                   w.prob, w.iSum + w.calls, w.iSum, w.calls, w.nDom, w.epc);
  }
  OS.flush();
}

double ModuleDumpCFG::getEdgeWeight(const BasicBlock *BB, unsigned idx) {
  double WeightPercent = 0.0;
  const Instruction *TI = BB->getTerminator();
  MDNode *WeightsNode = TI->getMetadata(LLVMContext::MD_prof);
  if (WeightsNode) {
    auto BranchProb = curBPI->getEdgeProbability(BB, idx);
    WeightPercent = ((double)BranchProb.getNumerator()) /
                    ((double)BranchProb.getDenominator());
  }
  return WeightPercent;
}

uint32_t ModuleDumpCFG::getIndirectCallWeight(const Instruction *I) {
  uint32_t weight = 1, tmp;
  const Function *F = I->getFunction();
  if (isa_and_nonnull<Function>(F)) {

    FunctionSet S = getIndirectCallees(I);

    for (Function *Callee : S) {
      if (wasExecuted(Callee))
        continue;
      tmp = FunctionDominators[Callee];
      if (tmp > weight) {
        weight = tmp;
      }
    }
  }
  return weight;
}

FunctionSet ModuleDumpCFG::getIndirectCallees(const Instruction *I) {
  const Function *F = I->getFunction();
  uint32_t line = I->getDebugLoc()->getLine();
  GraphEdge cur_edge = {F, line};

  auto it = CallGraphEdges.find(cur_edge);
  if (it != CallGraphEdges.end()) {
    FunctionSet S = it->second;
    return S;
  }
  return CallGraphEdges[{nullptr, 0}];
}

uint32_t ModuleDumpCFG::getDirectCallWeight(Function *F) {
  if (F->isIntrinsic()) return 0;
  if (F->isDeclaration()) return 1;

  if (isUniqCall(*F) && !wasExecuted(F))
    return FunctionDominators[F];

  return 0;
}

uint32_t ModuleDumpCFG::countCalls(const BasicBlock &BB,
		                   SmallPtrSetImpl<Function *> &directCalls) {
  uint32_t count = 0;
  for (const Instruction &I : BB) {
    if (auto *CB = dyn_cast<CallBase>(&I)) {
      if (Function *Callee = CB->getCalledFunction()) {
        /* DIRECT CALL */
        directCalls.insert(Callee);
//      count += getDirectCallWeight(Callee);
      } else {
        /* INDIRECT CALL  (what now?) */
        count += getIndirectCallWeight(&I);
      }
    }
  }
  return count;
}

void ModuleDumpCFG::readCallGraphFile(Module &M) {
  ErrorOr<std::unique_ptr<llvm::MemoryBuffer>> Text =
    MemoryBuffer::getFileAsStream(CallGraphFile);
  if (std::error_code EC = Text.getError()) {
    errs() << "Can't read " << CallGraphFile <<
      " " << EC.message() << "\n";
      return;
  }
  SmallVector<StringRef, 8> lines;
  (*Text)->getBuffer().split(lines, "\n",
                              /*MaxSplit=*/-1,
                              /*KeepEmpty=*/false);

  // Insert empty edge first.
  struct GraphEdge cg_edge = {nullptr, 0};
  CallGraphEdges[cg_edge];

  for (StringRef Line : lines) {
    SmallVector<StringRef, 6> data;
    uint32_t line_num = 0;
    Line.split(data, "\t", -1, true);
    auto caller_name = data[2].trim();
    auto callee_name = data[3].trim();
    auto line_num_str = data[4].trim();
    line_num_str.getAsInteger(10, line_num);
    auto caller_f = M.getFunction(caller_name);
    auto callee_f = M.getFunction(callee_name);
    errs() << "  cg-edge= " << caller_name << " >> "
            << callee_name << "  #" << line_num<< "\n";
    cg_edge = {caller_f, line_num};
    CallGraphEdges[cg_edge].insert(callee_f);
  }
}

void ModuleDumpCFG::DFS(Function *F,
                        SmallPtrSetImpl<Function *> &worklist,
                        SmallPtrSetImpl<Function *> &visited) {
  uint64_t count = 0;
  uint64_t tmp = 0;
  if (!worklist.contains(F)) return;
  visited.insert(F);
  errs() << "  starting on " << getFuncLabel(F) << "\n";

  for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
    Instruction &inst = *I;
    if (auto *CB = dyn_cast<CallBase>(&inst)) {
      if (Function *Callee = CB->getCalledFunction()){
        /* DIRECT CALL */
        if (!visited.contains(Callee)) {
          DFS(Callee, worklist, visited);
          count += getDirectCallWeight(Callee);
        }
      } else {
        /* INDIRECT CALL */
        FunctionSet IndCallSet = getIndirectCallees(&inst);
        for (Function *IndCallee : IndCallSet) {
          if (!visited.contains(IndCallee)) {
            DFS(IndCallee, worklist, visited);
            count += getIndirectCallWeight(&inst);
          }
        }
      }
    }
  }
  FunctionDominators[F] += count;
  errs() << "  Function " << F->getName() << " ";
  isUniqCall(*F, true);
  errs() << "\tsize " << FunctionDominators[F] << "\n";
  worklist.erase(F);
}

uint64_t ModuleDumpCFG::getFreq(const BasicBlock *BB) {
  return curBFI->getBlockFreq(BB).getFrequency();
}

uint64_t ModuleDumpCFG::getProfileCount(const BasicBlock *BB) {
  return curBFI->getBlockProfileCount(BB).getValueOr(0);
}



namespace {
  // Legacy PM implementation
struct DumpCFG : public ModulePass {
  static char ID;
  DumpCFG() : ModulePass(ID) {}

  bool runOnModule(Module &M) override {

    auto DTCallback = [this](Function &F) -> const DominatorTree * {
      return &this->getAnalysis<DominatorTreeWrapperPass>(F).getDomTree();
    };
    auto PDTCallback = [this](Function &F) -> const PostDominatorTree * {
      return &this->getAnalysis<PostDominatorTreeWrapperPass>(F)
                  .getPostDomTree();
    };
    auto BPICallback = [this](Function &F) {
      return &this->getAnalysis<BranchProbabilityInfoWrapperPass>(F).getBPI();
    };

    auto BFICallback = [this](Function &F) {
      return &this->getAnalysis<BlockFrequencyInfoWrapperPass>(F).getBFI();
    };

    auto dumper = ModuleDumpCFG();
    dumper.dumpModule(M, DTCallback, PDTCallback, BPICallback, BFICallback);

    return false;
  };

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesAll();
    AU.addRequired<DominatorTreeWrapperPass>();
    AU.addRequired<PostDominatorTreeWrapperPass>();
    AU.addRequired<BranchProbabilityInfoWrapperPass>();
    AU.addRequired<BlockFrequencyInfoWrapperPass>();
  }
}; // end of struct DumpCFG
}  // end of anonymous

char DumpCFG::ID = 0;

// Register the pass
static RegisterPass<DumpCFG>
  X(/*PassArg=*/"dump-cfg",
    /*Name=*/"Dump CFG edges",
    /*CFGOnly=*/false,
    /*is_analysis*/true);

static RegisterStandardPasses Y(
    PassManagerBuilder::EP_OptimizerLast,
    [](const PassManagerBuilder &Builder,
    legacy::PassManagerBase &PM) {PM.add(new DumpCFG()); }
);
