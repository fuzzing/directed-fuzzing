#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Format.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Analysis/BlockFrequencyInfo.h"


#include <iostream>

using namespace llvm;

#define DEBUG_TYPE "dump-stats"
#define MIN_FUNC_WAS_EXECUTED 0
#define MIN_BLOCK_PROFILE_CNT 50

typedef SmallPtrSet<Function *, 8> FunctionSet;
typedef SmallPtrSet<BasicBlock *, 8> BasicBlockSet;

static std::string getNodeLabel(const BasicBlock *Node, bool verbose = true) {
  std::string FuncName = "";
  if (verbose) {
    FuncName = Node->getParent()->getName().str() + ": ";
  }

  if (!Node->getName().empty())
    return FuncName + Node->getName().str();

  std::string Str;
  raw_string_ostream OS(Str);

  Node->printAsOperand(OS, false);
  return FuncName + OS.str();
}

static bool wasExecuted(const Function *F) {
  if (isa_and_nonnull<Function>(F)) {
    return F->getEntryCount().getCount() > MIN_FUNC_WAS_EXECUTED;
  }
  return false;
}

static uint32_t numCallees(Function &calledFunction) {
  FunctionSet Callees;
  for (User *U: calledFunction.users()) {
    if (auto CI = dyn_cast<CallInst>(U)) {
      Callees.insert(CI->getFunction());
    }
  }
  return Callees.size();
}

static void checkCallees(Function &F, SmallPtrSetImpl<Function *> &reachable) {
  FunctionSet Callees;
  Function *Callee;
  std::string Str;
  raw_string_ostream OS(Str);

  for (User *U : F.users()) {
    if (auto CI = dyn_cast<CallInst>(U)) {
      Callee = CI->getFunction();
      if (!wasExecuted(Callee) & reachable.contains(Callee)) {
        reachable.insert(&F);
      }
    }
  }
}


static void writeUnreachableFunctions(SmallPtrSetImpl<Function *> &noexec,
                                      SmallPtrSetImpl<Function *> &reachable) {
  std::error_code EC;
  raw_fd_ostream OS("/tmp/unreachable.txt", EC, sys::fs::OF_None);
  for (auto *F : noexec) {
    if (!reachable.contains(F)) {
      OS << F->getName() << "\t";
      for (User *U : F->users()) {
        if (auto CI = dyn_cast<CallInst>(U)) {
          OS << CI->getFunction()->getName() << ", ";
        }
      }
      OS << "\n";
    }
  }
  OS.flush();
}

namespace {
// Legacy PM implementation
struct DumpStats : public ModulePass {
  static char ID;
  DumpStats() : ModulePass(ID) {}

  bool runOnModule(Module &M) override {

    auto BFICallback = [this](Function &F) {
      return &this->getAnalysis<BlockFrequencyInfoWrapperPass>(F).getBFI();
    };

    uint32_t calls = 0;
    uint32_t direct_calls = 0;
    uint32_t indirect_calls = 0;
    uint32_t invoke_calls = 0;
    uint64_t noexecIns = 0;
    bool funcExec, blockExec;
    FunctionSet noexecFunctions;
    FunctionSet reachableFunctions;
    BasicBlockSet noexecBlocks;
    BasicBlockSet reachableBlocks;
    const BlockFrequencyInfo *curBFI;

    for (auto &F : M)  {
      if (F.isDeclaration()) continue;
      if (F.isIntrinsic()) continue;

      curBFI = BFICallback(F);
      funcExec = wasExecuted(&F);
      if (!funcExec) {
         noexecFunctions.insert(&F);
      }

      for (auto &B : F) {

        blockExec = curBFI->getBlockProfileCount(&B).getValueOr(0) > MIN_BLOCK_PROFILE_CNT;

        if (!blockExec) {
          noexecBlocks.insert(&B);
          noexecIns += B.sizeWithoutDebug();
        }

        for (auto &I : B) {
          if (CallBase *CB = dyn_cast<CallBase>(&I)){
            /* CALL instruction */
            calls++;
            if (Function *Callee = CB->getCalledFunction()) {
              /* DIRECT CALL */
              direct_calls++;
              if (funcExec & !wasExecuted(Callee))
                reachableFunctions.insert(Callee);
            } else {
              errs() << getNodeLabel(&B) << " -> ?? \n";
              indirect_calls++;
            }
          }
          if (InvokeInst *IV = dyn_cast<InvokeInst>(&I)) {
            invoke_calls++;
          }
        }
      }
    }

    uint32_t num_reachable = 0;
    while (num_reachable != reachableFunctions.size()) {
      num_reachable = reachableFunctions.size();
      for (auto &F : M) {
        if (F.isDeclaration()) continue;
	if (wasExecuted(&F)) continue;
        checkCallees(F, reachableFunctions);
      }
    }

    errs() << format("  calls (Di/In)  %d  ( %d / %d )\n", calls, direct_calls, indirect_calls);
    errs() << format("  invoke calls   %d\n", invoke_calls);
    errs() << format("  uncovered functions  %d\t reachable %d\n", noexecFunctions.size(), reachableFunctions.size());
    errs() << format("  uncovered blocks     %d\n", noexecBlocks.size());
    errs() << format("  uncovered instructions %d\n", noexecIns);

    errs() << "\n";
    writeUnreachableFunctions(noexecFunctions, reachableFunctions);
    
    return true;
  };

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    ModulePass::getAnalysisUsage(AU);
    AU.addRequired<BlockFrequencyInfoWrapperPass>();
    AU.setPreservesAll();
  }

}; // end of class DumpStats
}  // end of anonymous namespace

char DumpStats::ID = 0;

// Register the pass
static RegisterPass<DumpStats> 
  X(/*PassArg*/"dump-stats",
    /*Name=*/"Print module stats.",
    /*CFGOnly=*/false,
    /*is_analysis*/true);

static void registerDumpStatsPass(const PassManagerBuilder &,
                                  legacy::PassManagerBase &PM) {
  PM.add(new DumpStats());
}

static RegisterStandardPasses Y(
  PassManagerBuilder::EP_EarlyAsPossible, registerDumpStatsPass
);
