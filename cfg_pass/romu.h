// Romu Pseudorandom Number Generators
//
// Copyright 2020 Mark A. Overton
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// ------------------------------------------------------------------------------------------------
//
// Website: romu-random.org
// Paper:   http://arxiv.org/abs/2002.11331

#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

#define ROTL(d,lrot) ((d<<(lrot)) | (d>>(8*sizeof(d)-(lrot))))


static uint64_t xState, yState, zState;  // set to nonzero seed

uint64_t SplitMix64(uint64_t x) {
	uint64_t z = (x += 0x9e3779b97f4a7c15);
	z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
	z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
	return z ^ (z >> 31);
}

void romu_initialize_state(void) {
  struct timeval tv = {0};
  struct timezone tz = {0};

  gettimeofday(&tv, &tz);
  
  uint64_t seed = tv.tv_sec ^ tv.tv_usec ^ getpid();

  xState = SplitMix64(seed);
  yState = SplitMix64(seed ^ 0xfedcba1234);
  zState = SplitMix64(seed ^ 0x12345678ab);
}

static uint64_t romuDuoJr_random () {
   uint64_t xp = xState;
   xState = 15241094284759029579u * yState;
   yState = yState - xp;  yState = ROTL(yState,27);
   return xp;
}

static uint32_t romuTrio32_random () {
   uint32_t xp = xState, yp = yState, zp = zState;
   xState = 3323815723u * zp;
   yState = yp - xp; yState = ROTL(yState,6);
   zState = zp - yp; zState = ROTL(zState,22);
   return xp;
}

static inline double to_double(uint64_t x) {
  return (x >> 11) * 0x1.0p-53;
}

double next_rand_percent(void) {
  return to_double(romuDuoJr_random());
}

int64_t next_rand32(uint64_t limit) {
    uint64_t r;
    do {
        r = romuDuoJr_random();
    } while (r >= (UINT64_MAX - (UINT64_MAX % limit)));

    return r % limit;
}

uint32_t rand_idx(uint64_t *p, uint32_t n, uint64_t sum) {
  uint32_t i;
  int64_t s = next_rand32(sum);
  for (i = 0; i < n - 1 && (s -= p[i]) >= 0; i++);  
  return i;
}

uint32_t rand_idx_dbl(double *p, uint32_t n) {
  double s = next_rand_percent();
  uint32_t i;
  for (i = 0; i < n - 1 && (s -= p[i]) >= 0; i++);
  return i;
}
