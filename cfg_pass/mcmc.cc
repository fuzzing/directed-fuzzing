#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseMapInfo.h"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

#include "llvm/Analysis/BranchProbabilityInfo.h"
#include "llvm/Analysis/BlockFrequencyInfo.h"
#include "llvm/Support/BranchProbability.h"
#include "llvm/Support/CommandLine.h"

#include <assert.h>
#include <chrono>

#include "romu.h"

using namespace llvm;

#define DEBUG_TYPE "mcmc-analysis"

#define INIT_CALL_STACK_SIZE 64

static cl::opt<bool>
UseHeuristic("use-heuristic", cl::init(false), cl::Hidden,
             cl::desc("Use heuristic branch probabilities."));

static cl::opt<uint32_t> 
Iterations("iterations", cl::Hidden, cl::init(1), 
           cl::desc("Number of MCMC iterations performed."));

struct GraphEdge {
  BasicBlock *From;
  uint8_t idx;
};

bool operator==(GraphEdge LHS, GraphEdge RHS) {
  return LHS.From == RHS.From && LHS.idx == RHS.idx;
}

bool operator<(GraphEdge LHS, GraphEdge RHS) {
  return std::less<BasicBlock *>()(LHS.From, RHS.From) ||
  (LHS.From == RHS.From && LHS.idx < RHS.idx);
}

namespace llvm {

// Specialize DenseMapInfo For GraphEdge
template <> struct DenseMapInfo<GraphEdge> {
  static GraphEdge getEmptyKey() {
    return GraphEdge{DenseMapInfo<BasicBlock *>::getEmptyKey(),
    DenseMapInfo<uint8_t>::getEmptyKey()};
  }

  static GraphEdge getTombstoneKey() {
    return GraphEdge{DenseMapInfo<BasicBlock *>::getTombstoneKey(),
    DenseMapInfo<uint8_t>::getEmptyKey()};
  }

  static unsigned getHashValue(const GraphEdge  &Edge) {
    return DenseMapInfo<std::pair<BasicBlock *, uint8_t>>::getHashValue(
      std::make_pair(Edge.From, Edge.idx));
  }

  static bool isEqual(const GraphEdge &LHS, const GraphEdge &RHS) {
    return LHS== RHS;
  }
};
}  // end of namespace llvm


static std::string getNodeLabel(const BasicBlock *Node) {
  std::string FuncName = Node->getParent()->getName().str();

  if (!Node->getName().empty())
    return FuncName + ": " + Node->getName().str();

  std::string Str;
  raw_string_ostream OS(Str);

  Node->printAsOperand(OS, false);
  return FuncName + ": " + OS.str();
}

void dumpBlockInfo(BasicBlock *Succ, BlockFrequency *bf, BranchProbability *bp) {
    errs() <<   "  ==> " << getNodeLabel(Succ);
    errs() << "; freq: " << bf->getFrequency();
    errs() << "; prob: " << *bp;
    errs() << '\n';
}


namespace llvm {

class MCMCRandomWalks {
  using MapType = DenseMap<GraphEdge, uint32_t>;

private:
  Module *M;
  Function *entryFunc;
  Function *curFunc;
  BranchProbabilityInfo *curBPI;
  BlockFrequencyInfo *curBFI;
  MapType EdgeWeights;
  uint32_t max_call_stack = INIT_CALL_STACK_SIZE;

public:
  std::function<BlockFrequencyInfo *(Function &)> LookupBFI;
  std::function<BranchProbabilityInfo *(Function &)> LookupBPI;

  MCMCRandomWalks(Module *M,
                  function_ref<BlockFrequencyInfo *(Function &)> LookupBFI,
                  std::function<BranchProbabilityInfo *(Function &)> LookupBPI) {
  this->M = M;
  this->LookupBFI = LookupBFI;
  this->LookupBPI = LookupBPI;
  runRandomWalks();
  }

private:
  void runRandomWalks() {
    struct GraphEdge edge;

    for (auto &F : *M) {
      if (F.isDeclaration())
        continue;

//    curBFI = LookupBFI(F);
      curBPI = LookupBPI(F);

      for (auto &B : F) {
        Instruction *TI = B.getTerminator();
        if (TI && TI->getNumSuccessors() > 1) {
          for (unsigned I = 0, E = TI->getNumSuccessors(); I < E; I++) {
            edge.From = &B;
            edge.idx = I;
            EdgeWeights[edge] = getEdgeWeight(&B, TI, I);
          }
        }
      }

      if (F.getName() == "LLVMFuzzerTestOneInput") {
	      entryFunc = &F;
	      curFunc = &F;
      }
    }
    assert(entryFunc);

#ifndef NDEBUG
    StringRef ModuleName = M->getName();
    StringRef FuncName = entryFunc->getName();
    errs() << "[*] Function: " << FuncName <<  " in " << ModuleName << "\n";
#endif

    double *path_prob = new double[Iterations + 1];

    BasicBlock *BB = &entryFunc->getEntryBlock();
    for (int i=0; i < Iterations; i++) {
      path_prob[i] = analyze_path(BB);
    }

    delete [] path_prob;
  }

  uint64_t getEdgeWeight(const BasicBlock *BB, Instruction *TI, unsigned idx) {
    uint64_t weight = 0, md_weight = 0;
    MDNode *WeightsNode =TI->getMetadata(LLVMContext::MD_prof);

    if (WeightsNode) {
      ConstantInt *Weight =
        mdconst::dyn_extract<ConstantInt>(WeightsNode->getOperand(idx + 1));
        if (Weight)
          md_weight = Weight->getZExtValue();
    }

    // If UseHeuristic has been requested OR
    // if the metadata weight > 1 (profiling information available) OR
    // if the TI is an InvokeInst with multiple successors
    if (UseHeuristic || (WeightsNode &&  md_weight > 1) || isa<InvokeInst>(TI)) {
      weight = curBPI->getEdgeProbability(BB, idx).getNumerator();
    }

#ifndef NDEBUG
    BasicBlock *Succ = TI->getSuccessor(idx);
    errs() << " ;; BB " << getNodeLabel(BB) << " ==> " << getNodeLabel(Succ);
    errs() << "\tweight = " << weight << ", " << md_weight << "\n";
//  errs() << " freq= " << curBFI->getBlockFreq(BB).getFrequency();
//  errs() << " profCnt= " << curBFI->getBlockProfileCount(BB) << "\n";
#endif
    return weight;
  }

  double analyze_path(BasicBlock *BB) {
    Instruction *Term;
    BasicBlock::iterator II = BB->begin();
    uint32_t e = 0, i, choice;
    uint64_t count = 0, w[64];
    double Path = 0.0, pEdge = 0.0;
    uint64_t sum_weight = 2147483648;
    struct GraphEdge edge;

    SmallVector<std::pair<BasicBlock *, BasicBlock::iterator>, INIT_CALL_STACK_SIZE> CallStack;

    auto start = std::chrono::high_resolution_clock::now();

    while (BB) {
      // first check if this block contains a function call
      for (BasicBlock::iterator II_e = BB->end(); II != II_e; II++) {
        if (auto *CB = dyn_cast<CallBase>(II)) {
          if (Function *Callee = CB->getCalledFunction()) {
            if (Callee->isDeclaration()) {
#ifndef NDEBUG
              if (!Callee->isIntrinsic())
                errs() << "   Function : " << Callee->getName() << " is a declaration.\n";
#endif
            } else {
#ifndef NDEBUG
              errs() << "[*] " << getNodeLabel(BB) << " --- calling : " << Callee->getName() <<  " (" << CallStack.size() << ") \n";
#endif
	      if (max_call_stack < CallStack.size()) {
                max_call_stack = CallStack.size();
                errs() << "[-] max call stack size = " << max_call_stack << "\t edges: " << count << "\n";
	      }
	      if (max_call_stack > 1024) {
                errs() << "[-] max call stack size = " << max_call_stack << "\t edges: " << count << "\n";
		continue;
	      }
              CallStack.push_back(std::make_pair(BB, ++II));
              BB = &Callee->getEntryBlock();
              II = BB->begin();
              break;
            }
          } else {
            /* INDIRECT CALL */
          }
        }
      }
      // if were'not at the end of a block, loop and start at this instruction
      if (II != BB->end()) continue;

      if ((Term = BB->getTerminator())) {
        e = Term->getNumSuccessors();
      } else {
#ifndef NDEBUG
        errs() << "[-] skipping " << getNodeLabel(BB) << "\n";
#endif
        e = 0;
      }

      // case 0: no successors, return or exit
      if (e < 1) {
        if (CallStack.empty()) {
	  // we're done, exit now
          break;
        } else {
          // return from the current fuction, resume at II
          std::tie(BB, II) = CallStack.pop_back_val();
          continue;
        }
      }

      //  Only one out-going edge, loop
      if (e == 1) {
        BB = Term->getSuccessor(0);
        II = BB->begin();
        continue;
      }

#ifndef NDEBUG
      errs() << "    " << getNodeLabel(BB) << " with " << e << " successors ";
#endif

      i = 0;
      uint64_t sum = 0;
      for (BasicBlock *Succ : successors(BB)) {
        edge.From = BB;
        edge.idx = i;
        w[i] = EdgeWeights[edge];
        sum += w[i++];
        if (i > 64) {
          errs() << "[-] ERROR: more than 64 edges! " << e << " \n";
          return Path/count;
        }
      }
      if (sum == 0) {
        errs() << "    " << getNodeLabel(BB) << " with " << e << " successors ";
        errs() << format("==>   Path=%e, edges=%d", Path/count, count) << "\n";
        errs() << "[-] ERROR: no exit??\n ";
        return Path/count;
      }
      choice = rand_idx(w, i, sum);
      pEdge = ((double)w[choice]) / sum;


      Path += log(pEdge * 2.0);
      count++;
#ifndef NDEBUG
      errs() << format("==> pEdge= %.2f%%  Path=%e, edges=%d", pEdge * 100.0, Path/count, count) << "\n";
#endif

      BB = Term->getSuccessor(choice);
      II = BB->begin();
    }
    
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
#ifndef NDEBUG
    errs() << "Path is " << count << " edges long; normalized LLR = " << format("%.6f", Path/count) << "\n";
#else 
    errs() << format("%u,%.09f,%.4f ",count, Path/count, elapsed.count()) << "\n";
    #endif
    return Path/count;
  }

}; // end of class MCMCRandomWalks
}  // end of llvm namespace


namespace {
// Legacy PM implementation
struct MCMC : public ModulePass {
  static char ID;  // Pass identification, replacement for typeid
  MCMC() : ModulePass(ID) {
    romu_initialize_state();
  }

  bool runOnModule(Module &M) override {
    auto LookupBFI = [this](Function &F) {
      return &this->getAnalysis<BlockFrequencyInfoWrapperPass>(F).getBFI();
    };
    auto LookupBPI = [this](Function &F) {
      return &this->getAnalysis<BranchProbabilityInfoWrapperPass>(F).getBPI();
    };

    MCMCRandomWalks mcmcRandomWalks(&M, LookupBFI, LookupBPI);

    return false;
  }

  void print(raw_ostream &OS, const Module * = nullptr) const override{}

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesAll();
    AU.addRequired<BlockFrequencyInfoWrapperPass>();
    AU.addRequired<BranchProbabilityInfoWrapperPass>();
  }
}; // end of class MCMC
}  // end of anonymous namespace



char MCMC::ID = 0;
static RegisterPass<MCMC> X("mcmc", "Markov Chain Monte Carlo Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);

static RegisterStandardPasses Y(
    PassManagerBuilder::EP_OptimizerLast,
    [](const PassManagerBuilder &Builder,
       legacy::PassManagerBase &PM) { PM.add(new MCMC()); });
