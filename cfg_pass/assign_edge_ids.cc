#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/MDBuilder.h"
#include "llvm/IR/Instruction.h"

#include "llvm/Transforms/Instrumentation/PGOInstrumentation.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"


using namespace llvm;

uint32_t addEdgeMetadata(Module &M, Instruction *TI, uint32_t edge_id) {
  LLVMContext &C = M.getContext();
  MDBuilder MDB(C);
  uint32_t numSucc = TI->getNumSuccessors();
  if (numSucc == 0) numSucc++;
  SmallVector<Metadata *, 4> Vals(numSucc + 1);

  Vals[0] = MDB.createString("edge_ids");

  Type *Int32Ty = Type::getInt32Ty(C);
  for (unsigned i = 0; i != numSucc; ++i) {
    Vals[i + 1] = MDB.createConstant(ConstantInt::get(Int32Ty, edge_id++));
  }
  TI->setMetadata("pllr.edges", MDNode::get(C, Vals));
  return edge_id;
}


void createEdgeIDs(Module &M) {
  uint32_t edge_id = 0;
  for (auto &F : M) {
    if (F.isDeclaration())
      continue;
    for (auto &BB : F) {
      Instruction *TI = BB.getTerminator();
      edge_id = addEdgeMetadata(M, TI, edge_id);
    }
  }
}

namespace {
  // Legacy PM implmentation
  struct AssignEdgeIDs : public ModulePass {
    static char ID;
    AssignEdgeIDs() : ModulePass(ID) {}


    bool runOnModule(Module &M) override {
      createEdgeIDs(M);
      return true;
    }
  }; // end of struct AssignEdgeIDs
}    // end of anonymous namespace

char AssignEdgeIDs::ID = 0;

// Register the pass
static RegisterPass<AssignEdgeIDs>
  X(/*PassArg=*/"assign-edge-ids",
    /*Name=*/"Assign unique edge IDs", 
    /*CFGOnly=*/false,
    /*is_analysis=*/false);

static void registerAssignEdgeIDs(
  const PassManagerBuilder &, legacy::PassManagerBase &PM) {
    PM.add(new AssignEdgeIDs());
}

static RegisterStandardPasses Y(
  PassManagerBuilder::EP_FullLinkTimeOptimizationLast, registerAssignEdgeIDs
);

static RegisterStandardPasses Y0(
  PassManagerBuilder::EP_EnabledOnOptLevel0, registerAssignEdgeIDs
);