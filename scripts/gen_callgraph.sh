#!/bin/bash -eu

BM=${1:-freetype2}
HR=96h

# /dev/shm/fuzz/queues/fuzzing-96h-file

# cp -a callgraph.log orig-callgraph.log
TDIR="$(mktemp -d )"
FUZZER="$(realpath fuzzer)"
MERGED="$(realpath ../callgraph.log)"
I=100
echo "[*] using fuzzer : $FUZZER "
for h in bot01b  bot02b  bot03b  bot04b  bot05b  bot06b  bot07b  bot08b  bot09b; do
	QDIR=/dev/shm/fuzz/queues/fuzzing-${HR}-${BM}/${h}/*/outputs/*/queue
	printf "[*] %s \t %s \t" "$h" "$QDIR"
	pushd $TDIR  >/dev/null
	ls ${QDIR}/ > filelist
	wc -l filelist
	split -l 2000 filelist
	rm -f filelist
	popd >/dev/null
	pushd $QDIR >/dev/null
	for f in ${TDIR}/x*; do 
		echo "[*] filelist $f"
		cat $f | xargs $FUZZER  >/dev/null
		cp callgraph.log callgraph-${I}.log
		((I++))
	done
	cp callgraph-*.log ${TDIR}/
	rm -f callgraph*.log
	popd >/dev/null
done
cat ${TDIR}/callgraph-*.log | sort -k1,5 -u > ${TDIR}/callgraph.log
cp ${TDIR}/callgraph.log $MERGED

rm -rf ${TDIR}
