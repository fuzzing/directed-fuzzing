#!/bin/bash

ITER=${1:-100000}
BC=${2:-build/profile-file-v5.35/magic_fuzzer.bc}
LOGFILE=${3:-random_walks-$$.log}
# LOGFILE=/tmp/$(hostname)-random_walks.log
OPT=/usr/lib/llvm-12/bin/opt
# PASS_SO=build_dbg_pass/cfg_pass/libCFG-MCMC-Pass.so
PASS_SO=build_pass/cfg_pass/libCFG-MCMC-Pass.so
USE_H="-use-heuristic"

if [ ! -e $PASS_SO ]; then
    mkdir build_pass
    pushd build_pass >/dev/null
    cmake -G Ninja -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLLVM_DIR=/usr/lib/llvm-12/lib/cmake/llvm ..
    ninja
    popd >/dev/null
fi

if [ ! -e $LOGFILE ]; then
    echo "edges,path_llr,exec_s" > $LOGFILE
fi

$OPT -load $PASS_SO -mcmc -iterations $ITER -use-heuristic $BC  >/dev/null 2>>$LOGFILE
tail -n2 $LOGFILE
printf "[*] Finished $ITER iterations in $SECONDS secs, logfile length = "
wc -l $LOGFILE
