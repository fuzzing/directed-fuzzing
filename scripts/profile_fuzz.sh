#!/bin/bash -ue

BDIR="${1:-benchmarks/freetype2}"
HOURS="${2:-48}"
SEEDS="${3:-seeds}"
SOLUTIONS="${4:-does_not_exist}"
AFL_FUZZ="/opt/aflpp/afl-fuzz"

RESUME_FUZZING=no

DURATION="-V $(( 3600 * $HOURS + 5 ))"
BM="$(basename ${BDIR} )"
MEM_LIMIT="-m none"
EXEC_LIMIT="-t 9000"
EPOCH="$(date +%s)"
CMPLOG=""
COVLOG="-X ./bin/cov_fuzzer"

SAVEDIR="$(pwd)/.profdata"
FDIR="/dev/shm/fuzz/profile/${BM}/${HOURS}h_${EPOCH}"
rm -rf ${FDIR}
mkdir -p ${FDIR}
mkdir -p ${SAVEDIR}

export LLVM_PROFILE_FILE="raw/default-%m.profraw"

sudo ./scripts/afl-system-config || echo "[-] ERROR Failed to change system settings."

rsync -a ${BDIR}/ ${FDIR}/
# cp scripts/afl-fuzz ${FDIR}/bin

pushd $FDIR

add_solutions() {
# sleep 43000s
  mkdir -p outputs/manual/queue
  local i=0
  for sol in ${SOLUTIONS}/*; do
    local sid=$(printf "id:%06d" "$i")
    cp -v $sol "outputs/manual/queue/${sid},${sol##*/}" >> used_solutions.txt
    i=$((i+1))
  done
  touch outputs/manual/fuzzer_stats
}

if [[ -e bin/cmplog_fuzzer ]]; then
    CMPLOG="-c ./bin/cmplog_fuzzer"
fi

# Keep tmux windows after fuzzing
# tmux set-window-option -ag remain-on-exit on
echo $AFL_FUZZ -i ${SEEDS} -o outputs $MEM_LIMIT $EXEC_LIMIT $DURATION -M $BM -d $CMPLOG $COVLOG -- ./bin/aflpp_fuzzer -1000 | tee -a fuzz.log
$AFL_FUZZ -i ${SEEDS} -o outputs $MEM_LIMIT $EXEC_LIMIT $DURATION -M $BM -d $CMPLOG $COVLOG -- ./bin/aflpp_fuzzer -1000

if [[ $RESUME_FUZZING == "no" ]]; then
  llvm-profdata-12 merge -o ${SAVEDIR}/${BM}-${HOURS}h-${HOSTNAME}-${EPOCH}-profile.profdata raw/*
  exit 0
fi

echo "[*] sleeping for 30 minutes."
sleep 15m
pkill  -9 fuzzer

if [[ -d $SOLUTIONS ]]; then
  echo "[*] launching script to add solutions in %d seconds" "43200"
  touch used_solutions.txt
  add_solutions
fi

sleep 15m
echo "[*] resuming fuzzing."
export LLVM_PROFILE_FILE="raw/resume-%m.profraw"

COVLOG="-X ./mod/cov_fuzzer"
CMPLOG="-c ./mod/cmplog_fuzzer"
echo $AFL_FUZZ -i - -o outputs $MEM_LIMIT $EXEC_LIMIT $DURATION -M $BM -d $CMPLOG $COVLOG -- ./mod/aflpp_fuzzer -1000 | tee -a fuzz.log
$AFL_FUZZ -i - -o outputs $MEM_LIMIT $EXEC_LIMIT $DURATION -M $BM -d $CMPLOG $COVLOG -- ./mod/aflpp_fuzzer -1000

sleep 15m
pkill  -9 fuzzer

llvm-profdata-12 merge -o ${SAVEDIR}/${BM}-${HOURS}h-${HOSTNAME}-${EPOCH}-profile.profdata raw/*
