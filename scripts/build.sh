#!/bin/bash -ue

NPROC=16
HR="${HR:-24h}"

DATADIR="${PWD}/data/export/${HR}"
HOST="${1:-bot01a}"
PROFDATA_TEMPLATE="2023-03-29.profdata"

LIBDUMP="./build_pass/cfg_pass/libDumpCFG.so"

BENCHMARKS="${BENCHMARKS:-file freetype2 lcms libjpeg-turbo libpcap libxml2 proj4}"
# BENCHMARKS="libjpeg-turbo"
SECONDS=0
export LLVM_SYMBOLIZER_PATH=/usr/lib/llvm-12/bin/llvm-symbolizer

do_for_all() {
    action=${1:-}
    for tgt in ${BENCHMARKS}; do
        echo "[*] make -C benchmarks/${tgt} ${action} "
	rm -rf benchmarks/${tgt}/${action}-*
        make -j ${NPROC} -C benchmarks/${tgt} ${action} >/dev/null
    done
}

make_pgo_clean() {
  for tgt in ${BENCHMARKS}; do
     rm -rf benchmarks/${tgt}/profile.profdata
     rm -rf benchmarks/${tgt}/pgo-*
  done
}


make_pgo_bitcode_files() {
  for tgt in ${BENCHMARKS}; do
     printf "[*] compiling profile-guided optimization  %s\n" "$tgt"
#    local PDATA="/mnt/data/hosts/export/${HR}/${tgt}-${PROFDATA_TEMPLATE}"
     cp -v data/export/${HR}/${tgt}-2023-03-29.profdata benchmarks/${tgt}/profile.profdata
     make -C benchmarks/${tgt} pgo >/dev/null
  done
}

show_execution_entry_counts() {
  for tgt in ${BENCHMARKS}; do
     local PDATA="${DATADIR}/${tgt}-${PROFDATA_TEMPLATE}"
     local COVB=benchmarks/${tgt}/cov-*/fuzzer
     echo "[*] ====== $PDATA ====== "
#    llvm-cov-12 show --instr-profile $PDATA $COVB | grep -A4 -E  'LLVMFuzzerTestOneInput\([c ]'
     llvm-cov-12 export --summary-only -instr-profile $PDATA $COVB
#    llvm-cov-12 report -instr-profile $PDATA $COVB | (head -n 1 ; tail -n1)
 
  done
}

generate_coverage_reports() {
  for tgt in ${BENCHMARKS}; do
     local PDATA="${DATADIR}/${tgt}-${PROFDATA_TEMPLATE}"
     local COVB=benchmarks/${tgt}/cov-*/fuzzer
     llvm-cov-12 show --format=html -instr-profile $PDATA -output-dir=data/${tgt}/LCOV-${tgt} $COVB
     tar -C data/${tgt} -czf LCOV-${tgt}.tar.gz LCOV-${tgt}
  done
}

do_random_walks() {
  for tgt in ${BENCHMARKS}; do
 
     local PGO_BC=benchmarks/${tgt}/pgo-*/fuzzer.bc
     local LOGDATAFILE=data/${tgt}/${tgt}-random-walks-1M-48h-x9-$$.log
     mkdir -p $(dirname $LOGDATAFILE)
     ./scripts/run_random_walks.sh 1000000 $PGO_BC $LOGDATAFILE &
 
  done
}

dump_critical_edges() {
  local host=${1:-bot01a}
  for BM in ${BENCHMARKS};do
    NOW="$SECONDS"
    local LDATA="${PWD}/data/${BM}/ce"
    local PGO_BC=benchmarks/${BM}/pgo-*/fuzzer.bc
    local version="$(basename $(dirname $PGO_BC))"
    local callgraph_file=benchmarks/${BM}/callgraph.log
    local ce_csv="${LDATA}/${host}-edge-weights.csv"
    local ce_blocks="${LDATA}/${host}-blocks.log"
    mkdir -p ${LDATA}
    printf "[*] Processing  %-14s : %-20s ..." "${BM}" "${version}"
    USE_CALLGRAPH="-icfg ${callgraph_file}"
    opt-12 -load ${LIBDUMP} -dump-cfg ${USE_CALLGRAPH} -ce-out $ce_csv ${PGO_BC} >/dev/null 2>${ce_blocks}
    printf "\t ELAPSED %5d \t LAST %3d\n" "$SECONDS" "$(( SECONDS - $NOW ))"
    sort -t, -k7gr -k10g ${ce_csv} | head -n20 | column -t -s, > ${LDATA}/${host}-top20.csv
    sed -i "s,${version/pgo-/}/,,g" $ce_csv
  done
}

find_solutions() {
  local host=${1:-x9-12h-03-24}
  for BM in ${BENCHMARKS}; do
    local LDATA="${PWD}/data/${BM}/ce"
    local ce_csv="${LDATA}/${host}-edge-weights.csv"
    local DFSANB=benchmarks/${BM}/dfsan-*/fuzzer
    local QDIR=data/solutions/${BM}
    ./scripts/find_solutions.py $ce_csv $DFSANB $QDIR | tee -a data/${BM}/ce/found_solutions.txt
  done
}

do_analyze() {
  local logfile="${PWD}/${1:-critical_edges.md}"
  local analyze="${PWD}/scripts/analyze.py"
  rm -f $logfile
  for BM in ${BENCHMARKS}; do
    local LDATA="${PWD}/data/${BM}/ce"
    local DFSANB=${PWD}/benchmarks/${BM}/dfsan-*/fuzzer
    local QDIR=${PWD}/data/solutions/${BM}
    pushd $LDATA >/dev/null
    printf "\n%s\n\n" "## $BM critical edges" >> $logfile
    ${analyze} $DFSANB $QDIR | tee -a $logfile
    read -p " continue?  " continue_var
    popd >/dev/null
  done
}

usage() {
    cat <<EOF
${0} <command>

  download   1) download source for benchmarks
  coverage   2) compile coverage binaries
  aflpp      3) compile with AFL++ instrumentation
  install    4) install coverage and AFL++ binaries in 'bin' 
             5a) ./scripts/profile_fuzz.sh to generate profraw files
             5b) post process profraw with llvm-profdata to profdata files.
  pgo        6) run LLVM pass to generate bitcode with branch probabilities
  pllr       7) run LLVM pass to compile a final binary that calculates
                Path log-likelihood during runtime.
  dump-ce    8) run LLVM pass to dump critical edges based on PGO data.
  dfsan      9) run LLVM pass to compile DFSan binary that logs
                critical edge data 
  find      10) ./scripts/find_solutions.py to execute DFSan binary
                on a queue of inputs

Optional commands:
  check-profdata  check profdata files for execution counts
  html            generate llvm-cov html reports
  mcmc            do MCMC random walks 
EOF
}

case ${1:-} in
  download)
    do_for_all
    ;;
  coverage)
    do_for_all cov
    ;;
  aflpp)
    do_for_all aflpp
    do_for_all cmplog
    ;;
  install)
    do_for_all install
    ;;
  clean-pgo)
    make_pgo_clean
    ;;
  pgo)
    make_pgo_clean
    make_pgo_bitcode_files
    ;;
  pllr)
    do_for_all pllr
    ;;
  dfsan)
    do_for_all dfsan
    ;;
  dump-ce)
    dump_critical_edges "$2"
    ;;
  find)
    find_solutions "$2"
    ;;
  analyze)
    do_analyze
    ;;
  check-profdata)
    show_execution_entry_counts
    ;;
  html)
    generate_coverage_reports
    ;;
  mcmc)
    do_random_walks
    ;;
  *)
    usage
    ;;
esac


