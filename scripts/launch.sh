#!/bin/bash


# REGISTRY="registry.gitlab.com/rode0day/fuzzer-testing"
REGISTRY="anubis:5000/rode0day/fuzzer-testing"
TAG="${TAG:-16.04}"

FZ=aflgo
TGT="$1"
NF="2"
LOGDIR="${LOGDIR:-$HOME}"
DIMG="${REGISTRY}/afl_runner:${TAG}"
RUNC=docker
TLIM="$(( 60 * 60 * 24 ))"


usage() {
    echo "Usage: $0 [--fuzzer <fuzzer_name> [--pull] ] [-N <# instances>] [-D|--dict] [--limit <# seconds>] [--test] <target_name>"
    exit 1
}

check_docker_image() {
    docker image inspect "$1" >/dev/null || exit 1
}

while (( "$#" )); do
    case "$1" in
        --fuzzer)
            FZ=$2
            DIMG="${REGISTRY}/${FZ}_runner:${TAG}"
            shift 2
            ;;
        --limit)
            TLIM="$2"
            shift 2
            ;;
        --test)
            TLIM="$(( 60 * 15 ))"
            shift
            ;;
        --pull)
            docker pull $DIMG | tail -n2
            shift
            ;;
        -N)
            NF="$2"
            shift 2
            ;;
        -D|--dict)
            USE_DICT="dict"
            shift
            ;;
        -h|--help)
            usage
            ;;
        *)
            TGT="$1"
            shift
            ;;
    esac
done

check_docker_image $DIMG

cd $(dirname $0)/../
source $(dirname $0)/fuzz.sh
