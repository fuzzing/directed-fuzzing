#!/usr/bin/env python3
import os
import sys
from subprocess import run, TimeoutExpired
import tempfile

import pandas as pd

pd.set_option('display.max_colwidth', None)
pd.set_option('display.width', None)

extra_hosts = []
hosts = ['docker']


def read_file(filename, hostname='bot01a', ignore=False):
    headers = ['block_id', 'edge_id', 'edge_name', 'src_line', 'dst_line',
               'prob', 'weight', 'ins', 'calls', 'blocks', 'epc']
    df = pd.read_csv(filename, names=headers)
    df = df.sort_values(by=['weight', 'epc'], ascending=[False, True])
    df['host'] = hostname
    df['rank'] = df['weight'].rank(ascending=False).astype(int)
    df['util'] = df['prob'] * df['epc']
    if ignore:
        df['weight'] = 1
        df['epc'] = 1
    return df


def print_by_rank(df):
    grouped = df.groupby('rank')
    for i in range(1, 11):
        print(i, grouped.get_group(i)['edge_name'].unique().tolist())


def print_by_name(grouped):
    i = 0
    for name, group in grouped:
        ranks = str(group['rank'].unique().tolist())
        print("{}\t{:12}\t{}".format(group['weight'].count(), ranks, name))
        if i > 20:
            break
        i += 1


class Executor:
    def __init__(self, df, fuzzer, qdir, verbose=False):
        self.df = df.copy().head(25)
        self.exe = fuzzer
        self.qdir = qdir
        self.df['Label'] = -1
        self.df['Input'] = ""
        self.df['Sol'] = ""
        self.df = self.df.set_index('block_id', drop=False)
        self.verbose = verbose

    def find_solutions(self):
        with tempfile.TemporaryDirectory() as tdir:
            top20 = os.path.join(tdir, "blocklist.txt")
            self.df.to_csv(top20, sep=' ', columns=['block_id'], header=False, index=False)
#           top20 = filename.replace('edge-weights', 'top20')
            os.environ['PLLR_BLOCKLIST'] = top20
            os.environ['DFSAN_OPTIONS'] = 'warn_unimplemented=0'
            self.execute_testcases()
        return self.df

    def execute_testcases(self):
        for tc in os.listdir(self.qdir):
            try:
                self.execute_one(tc)
                if (self.df['Label'] >= 0).all():
                    return
            except KeyboardInterrupt:
                return

    def execute_one(self, filename):  # noqa
        new_env = os.environ.copy()
        tc_path = os.path.join(self.qdir, filename)
        try:
            p = run(args=[self.exe, tc_path],
                    timeout=20,
                    env=new_env,
                    capture_output=True,
                    universal_newlines=True)
            stderr = p.stderr
        except TimeoutExpired as p:
            print(f"  timeout on {filename} : {p}")
            stderr = p.stderr.decode('utf-8')

        for line in stderr.splitlines():
            if "pllr.CE," not in line:
                continue
            data = line.split(',')
            block_id = int(data[3])
            edge_id = int(data[4])
            label = int(data[5])
            if block_id not in self.df.index:
                continue
            matches = self.df['edge_id'] == edge_id
            if ((self.df.loc[block_id, 'edge_id'] == edge_id).any()):
                self.df.loc[matches, 'Sol'] = filename
                print(f" solved {edge_id:7} :: label {label} with input {filename}")
            # first consider where > 1 labels
            if isinstance(self.df.loc[block_id, 'Label'], pd.Series):
              if (label > self.df.loc[block_id, 'Label']).any():
                self.df.loc[block_id, 'Input'] = filename
                self.df.loc[block_id, 'Label'] = label
            elif label > self.df.loc[block_id, 'Label']:
              self.df.loc[block_id, 'Input'] = filename
              self.df.loc[block_id, 'Label'] = label
              if self.verbose:
                print(f" found {block_id:7} :: label {label} with input {filename}")


def read_dataframe():
    df_list = []
    for host in hosts:
        edge_csv = f"{host}-edge-weights.csv"
        if os.path.exists(edge_csv):
            df_list.append(read_file(edge_csv, host))
        else:
            print(f"  no file {edge_csv}")

    for host in extra_hosts:
        edge_csv = f"{host}-edge-weights.csv"
        if os.path.exists(edge_csv):
            df_list.append(read_file(edge_csv, host, ignore=True))
        else:
            print(f"  no file {edge_csv}")
    if len(df_list) == 0:
        print("  NO edge-weights found!")
        sys.exit(0)

    df = pd.concat(df_list)
    df = df[df['epc'] > 1]

    # df = df.sort_values(by=['weight', 'epc', 'edge_name'], ascending=[False, False, True])
    # df = df.groupby('edge_name', sort=False).filter(lambda x: len(x) > 4)
    grp = df.groupby('edge_name', sort=False, as_index=False).agg(
        block_id=('block_id', 'first'),
        edge_id=('edge_id', 'first'),
        rank=('rank', 'unique'),
        weight=('weight', 'first'),
        epc=('epc', 'first'),
        ins=('ins', 'first'),
        calls=('calls', 'first'),
        src=('src_line', 'first'),
        dst=('dst_line', 'first')
    )
    return grp


if __name__ == '__main__':
    df = read_dataframe()
    print_csv = False

    # print(grp.head(15).to_string(formatters={'epc': "{:.0f}".format, 'prob': "{:.9f}".format}))
    if len(sys.argv) > 2:
        fuzzer = sys.argv[1]
        qdir = sys.argv[2]
        e = Executor(df, fuzzer, qdir, verbose=False)
        new_df = e.find_solutions()
    else:
        new_df = df

    new_df = new_df.rename(columns={'block_id': 'b_id', 'edge_id': 'e_id'})
    if print_csv:
        print(new_df.head(25).to_csv(index=False))
        sys.exit(0)
    print(new_df.head(20).to_markdown(floatfmt=("", "", "", "", "", "0.0f", "", "", "0,.9f", "0,.2f"), index=False))
    print("\t weight={}\t ins={}\t calls={}".format(new_df['weight'].sum(), new_df['ins'].sum(), new_df['calls'].sum()))
