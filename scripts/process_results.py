#!/usr/bin/env python3

import json
import os
import platform
try:
    import pysqlite3 as sqlite3
except ImportError:
    import sqlite3
import sys

GRN = "\u001b[92m"
RST = "\u001b[0m"
QUERY_TC = (
    "SELECT b.address, b.size AS b_size, tc.filename, tc.reltime, tc.size AS tc_size,"
    " tc.new_blocks, tc.new_edges, tc.edges, tc.blocks, tc.returncode "
    "FROM foundblock fb "
    "JOIN block b ON fb.block_id = b.id "
    "JOIN testcase tc ON tc.id = fb.testcase_id "
    "WHERE ? <= b.address + b.size AND ? >= b.address "
    "ORDER by tc.reltime"
    )

QUERY_LAST = (
    "SELECT reltime, blocks, edges FROM testcase ORDER BY reltime DESC LIMIT 1"
    )

job_desc_re = r"j500_8448.120 file-14458 M=0 N=1 aflpp 2019.02 HOST=top04b-docker-file; CPU= Intel(R) Xeon(R) CPU E5-2670 v2 @ 2.50GHz; x40; free=233.026;"  # noqa
job_desc_re = r"(\S+) \S+ M=(\d+) N=(\d+) (\S+) \S+ HOST=(\S+);"


def last_testcase(db):
    cur = db.cursor()
    cur.execute(QUERY_LAST)
    row = cur.fetchone()
    if row is None:
        return {}
    data = {
        'last_path': row['reltime'],
        'total_blocks': row['blocks'],
        'total_edges': row['edges']
    }
    return data


def find_testcase(db, block):
    results = []
    cur = db.cursor()
    cur.execute(QUERY_TC, (block, block))
    for row in cur:
        path = row['filename'].split('/')
        queue = path[path.index('outputs') + 1]
        data = {
            'reltime': row['reltime'],
            'queue': queue,
            'filename': path[-1],
            'target_addr': block,
            'b_addr': row['address'],
            'b_size': row['b_size'],
            'tc_size': row['tc_size']
        }
        results.append(data)

    if len(results) == 0:
        return {}
    first = results[0]
    first['finds'] = len(results)
    return first


class TrialResults:
    def __init__(self, target_dir):
        self.cmp_id = 0
        job_dir, self.target = os.path.split(target_dir)
        fdir, self.job_id = os.path.split(job_dir)
        _, fuzzer = os.path.split(fdir)
        self.fuzzer = fuzzer
        self.host = platform.node()
        self.target_blocks = []
        self.data = {}
        self.FDIR = target_dir
        self.monitor_db = os.path.join(target_dir, 'outputs/monitor_cov.db')

        # Load the job config file
        self.get_target_blocks()
        # Load the compare data
        self.get_compare_data()

        # Query DB for relative discovery time
        self.query_db()

    def get_cmp_id_from_pruned(self):
        targets_pruned = os.path.join(self.FDIR, 'parm/bin/targets.pruned.json')
        with open(targets_pruned) as f:
            data = json.load(f)
            self.cmp_id = data['targets'][0]
        return data['targets'][0]

    def get_target_blocks(self):
        job_file = os.path.join(self.FDIR, '{}_job.json'.format(self.fuzzer))
        with open(job_file) as f:
            self.job_config = json.load(f)
        self.target_blocks.extend(self.job_config['target_blocks'])
        # Find the Angora compare-id
        self.get_cmp_id_from_job()

    def get_cmp_id_from_job(self):
        job_name = self.job_config['name']
        if 'AAAA' in job_name:
            return self.get_cmp_id_from_pruned()
        _, tgt, _ = job_name.split(maxsplit=2)
        target, cmp_id = tgt.split('-')
        try:
            self.cmp_id = int(cmp_id)
        except ValueError:
            print("  job_name = {}\t {}".format(job_name, self.FDIR))
            return
        return int(cmp_id)

    def get_compare_data(self):
        cmp_json_file = 'targets/target-cmp_id-{}.json'.format(self.cmp_id)
        if not os.path.isfile(cmp_json_file):
            print("Compare data file not found ({}).".format(cmp_json_file))
            return
        with open(cmp_json_file) as f:
            data = json.load(f)
        self.data.update(data)

    def query_db(self):
        discovery_time = -1
        db = sqlite3.connect(self.monitor_db)
        db.row_factory = sqlite3.Row
        self.data['finds'] = list()
        for block in self.target_blocks:
            data = find_testcase(db, block)
            if len(data) > 0:
                self.data['finds'].append(data)
                if data['reltime'] > discovery_time:
                    discovery_time = data['reltime']
        self.data.update(last_testcase(db))
        db.close()
        if len(self.data['finds']) == len(self.target_blocks):
            self.data['dtime'] = discovery_time
        if discovery_time == -1:
            return
        sys.stdout.write("discovered cmp_id {} in {}{:6}{} seconds.".format(
            self.cmp_id, GRN, discovery_time, RST))

    def write_results(self, data_dir):
        rfile = "{}/{}.{}.{}.{}.json".format(
            data_dir, self.target, self.cmp_id, self.fuzzer, self.job_id, self.host)
        self.data['fuzzer'] = self.fuzzer
        self.data['hostname'] = self.host
        self.data['job_id'] = self.job_id
        with open(rfile, 'w') as f:
            json.dump(self.data, f, indent=4)


if __name__ == '__main__':
    fuzz_dir = sys.argv[2]
    trial = TrialResults(fuzz_dir)
    trial.write_results(sys.argv[1])
