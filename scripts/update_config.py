#!/usr/bin/env python3

import json
import os
import sys


def use_pruned(base_dir):
    targets_pruned = os.path.join(base_dir, 'parm/bin/targets.pruned.json')
    print("pruned= '{}'".format(targets_pruned))
    with open(targets_pruned) as fp:
        data = json.load(fp)
        cmp_id = data['targets'][0]
    return cmp_id


if len(sys.argv) == 3:
    parmesan_config = sys.argv[1]
    fuzzer_config = sys.argv[2]
    base_dir, file_path = os.path.split(parmesan_config)

    with open(parmesan_config) as fp:
        parmesan_data = json.load(fp)

    with open(fuzzer_config) as fp:
        fuzzer_data = json.load(fp)

    if 'angora_cmp_id' in parmesan_data:
        cmp_id = parmesan_data['angora_cmp_id']
        target_blocks = parmesan_data['target_blocks']
    elif 'cmp_id' in parmesan_data:
        cmp_id = parmesan_data['cmp_id']
        target_blocks = parmesan_data['addresses']
    else:
        print("Do not have compare id, bailing!")
        sys.exit(1)

    fuzzer_data['target_blocks'] = target_blocks
    fuzzer_data['name'] = fuzzer_data['name'].replace('AAAA', str(cmp_id))
    fuzzer_data['description'] = fuzzer_data['description'].replace('AAAA', str(cmp_id))

    with open(fuzzer_config, 'w') as fp:
        json.dump(fuzzer_data, fp, indent=4)

if len(sys.argv) == 1:
    parm_config = 'benchmarks/file/parmesan_job.json'
    aflpp_config = '/home/localuser/archive/df_configs/file/job500/aflpp_job.json'
    new_config   = '/home/localuser/archive/df_configs/file/job500/parmesan_job.json'

    with open(parm_config) as f:
        fuzzer_data = json.load(f)

    with open(new_config) as f:
        aflpp_data = json.load(f)

    _, name_str = aflpp_data['name'].split('-')
    cmp_str, _ = name_str.split()
    cmp_id = int(cmp_str)
    target_blocks = aflpp_data['target_blocks']
    if len(target_blocks) < 1 or cmp_id < 1:
        print(f"failed to parse {cmp_id} \n target blocks {target_blocks}")

    fuzzer_data['angora_cmp_id'] = cmp_id
    fuzzer_data['target_blocks'] = target_blocks
    fuzzer_data['name'] = fuzzer_data['name'].replace('AAAA', str(cmp_id))
    fuzzer_data['description'] = fuzzer_data['description'].replace('AAAA', str(cmp_id))

    with open(new_config, 'w') as f:
        json.dump(fuzzer_data, f, indent=4)
