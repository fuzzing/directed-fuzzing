#!/usr/bin/env python3

import csv
import os
import sys

import pandas as pd

from analyze import Executor

pd.set_option('display.max_colwidth', None)
pd.set_option('display.width', None)
pd.options.display.float_format = '{:.7f}'.format

names = ['block_id', 'edge_id', 'func_edge', 'src', 'dst', 'prob', 'weight', 'ins', 'calls', 'blocks', 'epc']
display_names = ['func_edge', 'src', 'dst', 'ins', 'calls', 'blocks', 'weight', 'epc', 'Label', 'Input', 'Sol']
# LOG format
# type     edges,weight,block_id,edge_id,label
# pllr.CE, 12012,0.249405427,30815,30816,0


def read_top20(filename):
    results = []
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=names)
        for row in reader:
            results.append(row)

    sres = sorted(results, key=lambda i: i['weight'], reverse=True)
    return sres[:50]


def read_pd20(filename):
    df = pd.read_csv(filename, names=names)
    df = df.sort_values(['weight'], ascending=False)
    df['Label'] = -1
    df['Input'] = ""
    return df.head(50)


edgelist = sys.argv[1]
fuzzer = sys.argv[2]
qdir = sys.argv[3]

if os.path.exists(edgelist):
    ce = read_pd20(edgelist)

    e = Executor(ce, fuzzer, qdir)
    new_df = e.find_solutions()
    new_df = new_df[display_names]
    print(new_df)
