#!/bin/bash -eu

PRJ=${1:-2021-04-23-7d-paper}
BENCHMARKS=${BENCHMARKS:-libxml2-v2.9.2 lcms-2017-03-21 freetype2-2017 proj4-2017-08-14 libjpeg-turbo-07-2017 libjpeg-turbo-07-2017 libpcap_fuzz_both}
FZR="aflplusplus"
GSBK="gs://fuzzbench-data"



mkdir -p ${PRJ}/coverage-binaries

for BM in $BENCHMARKS; do
	tarfile="coverage-build-${BM}.tar.gz"
	outtar="${PRJ}/coverage-binaries/${tarfile}"
	if [[ ! -e $outtar ]]; then
		gsutil cp ${GSBK}/${PRJ}/coverage-binaries/${tarfile} ${outtar}
	fi

	outdir="${PRJ}/experiment-folders/${BM}-${FZR}"
	mkdir -p ${outdir}
	TRIALS="$(gsutil ls ${GSBK}/${PRJ}/experiment-folders/${BM}-${FZR} )"
	for trial in $TRIALS; do
		tarfile=corpus-archive-0673.tar.gz
		if [[ ! -e ${outdir}/$tarfile ]]; then
			gsutil cp ${trial}corpus/corpus-archive-0673.tar.gz ${outdir}
			tar -C ${outdir} -xf ${outdir}/$tarfile
		fi
		break
	done
done
