#!/usr/bin/env python3
import glob
import json
import os
from subprocess import run
import pandas as pd

DATESTR = "2021-06-25"

BASEDIR = '/opt/fuzz/directed-fuzzing/benchmarks'
BENCHMARKS = {
    'checker': f"{BASEDIR}/checker/cov-checker-v0/fuzzer",
    'file': f"{BASEDIR}/file/cov-file-FILE5_38/fuzzer",
    'file_oss': f"{BASEDIR}/file_oss/cov-file-FILE5_38/fuzzer",
    'freetype2': f"{BASEDIR}/freetype2/cov-freetype-2.9/fuzzer",
    'lcms': f"{BASEDIR}/lcms/cov-lcms2.10/fuzzer",
    'lcms_01': f"{BASEDIR}/lcms_01/cov-lcms2.10/fuzzer",
    'lcms_c': f"{BASEDIR}/lcms_01/cov-lcms2.10/fuzzer",
    'libjpeg-turbo': f"{BASEDIR}/libjpeg-turbo/cov-libjpeg-turbo-2.0.6/fuzzer",
    'libjpeg-turbo_01': f"{BASEDIR}/libjpeg-turbo_01/cov-libjpeg-turbo-2.0.6/fuzzer",
    'libjpeg-turbo_c': f"{BASEDIR}/libjpeg-turbo_01/cov-libjpeg-turbo-2.0.6/fuzzer",
    'libpcap': f"{BASEDIR}/libpcap/cov-libpcap-1.10.0/fuzzer",
    'libpcap_ff': f"{BASEDIR}/libpcap_ff/cov-libpcap-1.10.0/fuzzer",
    'libpcap_fp': f"{BASEDIR}/libpcap_fp/cov-libpcap-1.10.0/fuzzer",
    'libxml2': f"{BASEDIR}/libxml2/cov-libxml2-v2.9.8/fuzzer",
    'libxml2_01': f"{BASEDIR}/libxml2_01/cov-libxml2-v2.9.8/fuzzer",
    'libxml2_c': f"{BASEDIR}/libxml2_01/cov-libxml2-v2.9.8/fuzzer",
    'proj4': f"{BASEDIR}/proj4/cov-PROJ-5.2.0/fuzzer"
}


def get_json_data(profdata, cov_binary):
    p = run(args=[
        'llvm-cov-12',
        'export',
        '-summary-only',
        '-instr-profile',
        profdata,
        cov_binary],
        universal_newlines=True,
        capture_output=True)
    j = json.loads(p.stdout)
    return j['data'][0]['totals']


def parse_record(profdata, tgt=None):
    filename = os.path.basename(profdata)
    nameArray = filename.split('-')
    if tgt is None:
      tgt = nameArray[0]
      hrs = nameArray[-5]
    else:
      hrs = nameArray[0]
    host, _ = nameArray[-1].split('.')
    cov_binary = BENCHMARKS[tgt]
    data = get_json_data(profdata, cov_binary)
    result = {
        'src': filename,
        'tgt': tgt,
        'host': host,
        'hrs': hrs,
        'lines': data['lines']['covered'],
        'regions': data['regions']['covered'],
        'branches': data['branches']['covered'],
        'functions': data['functions']['covered'],
        'instantiations': data['instantiations']['covered']
    }
    return result


def save_merged():
    r = list()
    for hrs in ['12h', '24h', '48h', '96h']:
        pdata = glob.glob(f"/mnt/data/hosts/export/{hrs}/*-{hrs}-*-x*.profdata")
        for profdata in pdata:
            r.append(parse_record(profdata))
    df = pd.DataFrame(r)
    df.sort_values(['tgt', 'regions']).to_csv('data/coverage/merged_cov_data.csv', index=False)


# /mnt/data/hosts/export/freetype2/12h-2021-03-24-bot10b.profdata
def get_individual():
    r = list()
    for tgt in ['file', 'freetype2', 'lcms', 'lcms_01', 'lcms_c', 'libjpeg-turbo', 'libjpeg-turbo_01', 'libjpeg-turbo_c', 'libpcap', 'libxml2', 'libxml2_01', 'libxml2_c', 'proj4']:  # noqa
        pdata = glob.glob(f"/mnt/data/hosts/export/{tgt}/24h-{DATESTR}*.profdata")
        for profdata in pdata:
            r.append(parse_record(profdata, tgt))
    return r


pdata = '/mnt/data/hosts/export/96h/file-96h-2021-03-24-x9.profdata'
covb = 'benchmarks/file/cov-file-FILE5_38/fuzzer'


def save_individual_coverage():
    d = get_individual()
    df = pd.DataFrame(d)
    df.sort_values(['tgt', 'regions']).to_csv(f'data/coverage/{DATESTR}-24h-cov-data.csv', index=False)


save_individual_coverage()
# save_merged()
# print(df.sort_values(['tgt','regions']))
