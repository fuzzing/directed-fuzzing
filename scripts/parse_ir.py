#!/usr/bin/env python3
import argparse
from collections import defaultdict
import json
import os
import random
import re
import sys

MIN_PYTHON = (3, 7)
if sys.version_info < MIN_PYTHON:
    sys.exit("Python %s.%s or later is required.\n" % MIN_PYTHON)

# %35   = call i32 @__angora_trace_cmp(i32 %33,   i32 14,  i32 %34,   i64 %32,   i64 0),     !dbg !1249, !nosanitize !4
# %1261 = call i32 @__angora_trace_cmp(i32 %1259, i32 164, i32 %1260, i64 %1257, i64 %1258), !dbg !1479, !nosanitize !4

# %146 = call i64 @__angora_trace_switch(i32 32, i32 %145, i64 %144), !dbg !1269, !nosanitize !4
#  %22 = call i64 @__angora_trace_switch(i32 17850, i32 %21, i64 %20), !dbg !30003, !nosanitize !4

# !642 = !DIFile(filename: "readcdf.c", directory: "/data/file-FILE5_35/src")

# !25232 = distinct !DILexicalBlock(scope: !25204, file: !818, line: 727, column: 6)
# !23232 = !DILexicalBlockFile(scope: !22983, file: !818, discriminator: 0)

# !30212 = !DILocation(line: 162, column: 2, scope: !30213)
# !1261 = !DILocation(line: 208, column: 8, scope: !1117)


# !29981 = distinct !DISubprogram(name: "get_next_format_from_precision", scope: !1091, file: !1091, line: 63, type: !29940, isLocal: true, isDefinition: true, scopeLine: 64, flags: DIFlagPrototyped, isOptimized: false, unit: !1090, retainedNodes: !4)  # noqa

BLOCKLIST = {
    'filenames': set(['apprentice.c', 'file.c']),
    'functions': set()
}


#atc = re.compile("%\d+ = call i32 @__angora_trace_cmp\(i32 %\d+, i32 (\d+), i32 %\d+, i64 %?\d+, i64 .?\d+\),(?: !dbg !(\d+),)? !nosanitize !\d+.*")  # noqa
atc = re.compile(r"%\d+ = call i32 @__angora_trace_cmp\(i32 %\d+, i32 (\d+), i32 %\d+, [^)]*[)]?\),(?: !dbg !(\d+),)? !nosanitize !\d+.*")  # noqa
atsw = re.compile(r"%\d+ = call i64 @__angora_trace_switch\(i32 (\d+), i32 %\d+, i64 %\d+\), !dbg !(\d+), !nosanitize !\d+.*")  # noqa
re_loc = re.compile(r"!(\d+) = !DILocation\(line: (\d+),(?: column: (\d+),)? scope: !(\d+)\).*")
re_file = re.compile(r'!(\d+) = !DIFile\(filename: "([^"]*)", directory: "([^"]*)"\).*')
re_block = re.compile(r"!(\d+) = distinct !DILexicalBlock\(scope: !(\d+), file: !(\d+), line: (\d+), column: \d+\)")
re_blockfile = re.compile(r"!(\d+) = !DILexicalBlockFile\(scope: !(\d+), file: !(\d+), discriminator: (\d+)\)")
re_func_name = re.compile(r"[^@]*@([^(]+)\(.*")
re_subprogram = re.compile(r'!(\d+) = distinct !DISubprogram\(name: "[^"]+", scope: !(\d+), file: !(\d+), line: (\d+), [^)]*\)')  # noqa


def ordered_load_json(stream):
    return json.load(stream, object_pairs_hook=OrderedDict)


class AngCmp:
    def __init__(self, cmp_id, dbg_id):
        self.cmp_id = cmp_id
        self.cmp_dbg_id = dbg_id
        self.line_num = -1
        self.filename = None
        self.func_name = None

    def __repr__(self):
        return str(self.cmp_id)

    @property
    def oneline(self):
        return f"cmp_id = {self.cmp_id} {self.filename}:{self.line_num} in function {self.func_name}"


class AngoraIR:
    def __init__(self, filename):
        self.cmps = dict()
        self.files = dict()
        self.functions = defaultdict(dict)
        self.lines = dict()
        self.blocks = dict()
        self.process_file(filename)

    def check_re_match(self, re_obj, text, ret):
        m = re_obj.match(text.strip())
        if m is None:
            print(f"Error on {text.strip()}")
            return ret
        return m

    def line_angora_trace_cmp(self, t):
        m = self.check_re_match(atc, t, (0, 0))
        if isinstance(m, tuple):
            return m
        return map(int, m.groups(default=-1))

    def line_angora_trace_switch(self, t):
        m = self.check_re_match(atsw, t, (0, 0))
        if isinstance(m, tuple):
            return m
        return map(int, m.groups(default=-1))

    def line_DIFile(self, t):
        m = self.check_re_match(re_file, t, (0, '', ''))
        if isinstance(m, tuple):
            return m
        dbgId, filename, directory = m.groups()
        return (int(dbgId), filename, directory)

    def line_DILocation(self, t):
        m = self.check_re_match(re_loc, t, (0, 0, 0, 0))
        if isinstance(m, tuple):
            return m
        return map(int, m.groups(default=-1))

    def line_DILexicalBlock(self, t):
        m = self.check_re_match(re_block, t, (0, 0, 0, 0))
        if isinstance(m, tuple):
            return m
        return map(int, m.groups(default=-1))

    def line_DILexicalBlockFile(self, t):
        m = self.check_re_match(re_blockfile, t, (0, 0, 0))
        if isinstance(m, tuple):
            return m
        return map(int, m.groups(default=-1))

    def line_DISubprogram(self, t):
        m = self.check_re_match(re_subprogram, t, (0, 0, 0, 0))
        if isinstance(m, tuple):
            return m
        dbg_id, scope, file_id, line_num = m.groups(default=-1)
        return (int(dbg_id), int(scope), int(file_id), int(line_num))

    def read_file(self, fp):
        func_name = ""
        for line in fp:
            if '; Function' in line:
                next_line = next(fp)
                func_name = re_func_name.match(next_line).group(1)
            if 'angora_trace_cmp' in line:
                cmpId, dbgId = self.line_angora_trace_cmp(line)
                angora_compare = AngCmp(cmpId, dbgId)
                self.functions[func_name][cmpId] = angora_compare
                self.cmps[cmpId] = angora_compare
            if 'angora_trace_switch' in line:
                cmpId, dbg_id = self.line_angora_trace_switch(line)
                angora_compare = AngCmp(cmpId, dbg_id)
                self.functions[func_name][cmpId] = angora_compare
                self.cmps[cmpId] = angora_compare
            if ' !DILocation' in line:
                dbgId, line_num, col, scope = self.line_DILocation(line)
                self.lines[dbgId] = (line_num, col, scope)
            if ' !DIFile' in line:
                dbgId, filename, directory = self.line_DIFile(line)
                self.files[dbgId] = filename
            if ' !DILexicalBlock(' in line:
                dbgId, scope, fileNum, line_num = self.line_DILexicalBlock(line)
                self.blocks[dbgId] = (scope, fileNum, line_num)
            if ' !DILexicalBlockFile(' in line:
                dbgId, scope, fileNum, line_num = self.line_DILexicalBlockFile(line)
                self.blocks[dbgId] = (scope, fileNum, line_num)
            if ' !DISubprogram(' in line:
                dbgId, scope, file_num, line_num = self.line_DISubprogram(line)
                self.blocks[dbgId] = (scope, file_num, line_num)

    def process_file(self, filename):
        with open(filename) as fp:
            self.read_file(fp)

        print(f"[*] {len(self.functions)} functions identified.")
        min_id = min(self.cmps.keys())
        max_id = max(self.cmps.keys())
        print(f"[*] {len(self.cmps)} Angora compare injections ({min_id}, {max_id}).")

        for func_name, f in self.functions.items():
            for cmp_id, ac in f.items():
                if ac.cmp_dbg_id in [-1, 0]:
                    continue
                line_num, line_col, line_scope = self.lines[ac.cmp_dbg_id]
                ac.line_num = line_num
                try:
                    block_scope, block_file_num, block_line_num = self.blocks[line_scope]
                except IndexError:
                    print("  no block for ", line_scope, line_num, ac.cmp_dbg_id, ac.cmp_id)
                    continue
                ac.filename = self.files[block_file_num]
                ac.func_name = func_name

    def check_all_compares(self):
        compares = [int(line) for line in open('all_compares') if 'cmp' not in line and int(line) not in self.cmps]
        print(sorted(compares))
        print()

    def get_random_compare(self):
        blocked = True
        while blocked:
            cmp_id = random.choice(list(self.cmps.keys()))
            compare = self.cmps[cmp_id]
            blocked = (compare.filename in BLOCKLIST['filenames']) or \
                (compare.func_name in BLOCKLIST['functions'])
        return compare

    def print_compare(self, cmp_id=None):
        if cmp_id is None:
            cmp_id = random.choice(list(self.cmps.keys()))
        ac = self.cmps[cmp_id]
        print(ac.oneline)

    def print_function_compares(self, func_name='do_note_core'):
        function = self.functions[func_name]
        print(function.keys())
        for cmpid, ac in function.items():
            print(ac.oneline)


def get_coverage_address(coverage_binary, src, linenum):
    import dwarf_lineprogram_filenames as dlf
    locs = {(src, linenum): {'addrs': list()}}
    dlf.process_file(coverage_binary, locs)
    addrs = set([addr for v in locs.values() for addr in v['addrs']])
    print(sorted(addrs))
    return sorted(addrs)


def modify_example_job(args, cmp_id, addresses):
    with open(args.config) as fp:
        j = json.load(fp)
    j['target_blocks'] = sorted(addresses)
    j['name'] = j['name'].replace('AAAA', str(cmp_id))
    j['description'] = j['description'].replace('AAAA', str(cmp_id))
    j['angora_cmp_id'] = cmp_id
    new_name = os.path.join('targets', f"parmesan_job.{cmp_id}.json")
    with open(args.config, 'w') as fp:
        json.dump(j, fp, indent=4)

    base_path, llvm_ir_file = os.path.split(args.llvm_ir)
    full_targets_json = os.path.join(base_path, 'targets.json')
    with open(full_targets_json) as fp:
        j = json.load(fp)
    j['targets'] = [cmp_id]
    new_name = os.path.join('targets', f"targets.pruned.{cmp_id}.json")
    pruned_targets_json = os.path.join(base_path, 'targets.pruned.json')
    with open(pruned_targets_json, 'w') as fp:
        json.dump(j, fp)


def parse_args():
    parser = argparse.ArgumentParser(description="Maps Angora compare IDs to source line numbers.")
    parser.add_argument('-i', '--llvm-ir', required=True, help="<prog>.fast.ll generated by Parmesan.")
    parser.add_argument('--compare', default=None, help="Angora compare ID.")
    parser.add_argument('-b', '--binary', default=None,
                        help="uninstrumented coverage binary (for target <-> address mapping.")
    parser.add_argument('-c', '--config', help="<fuzzer>_job.json fuzzing config file.")
    parser.add_argument('-u', '--update', action='store_true', default=False,
                        help="update targets.pruned.json with new targets.")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    angora_ll = AngoraIR(args.llvm_ir)
    rand_cmp = None
    if args.compare:
        rand_cmp = angora_ll.cmps.get(int(args.compare))
    if rand_cmp is None:
        rand_cmp = angora_ll.get_random_compare()
    print(rand_cmp.oneline)
    if args.binary:
        addrs = get_coverage_address(args.binary, rand_cmp.filename, rand_cmp.line_num)
        if args.update:
            modify_example_job(args, rand_cmp.cmp_id, addrs)
    new_name = f"targets/target-cmp_id-{rand_cmp.cmp_id}.json"
    with open(new_name, 'w') as fp:
        data = vars(rand_cmp)
        data['addresses'] = addrs
        json.dump(data, fp)
