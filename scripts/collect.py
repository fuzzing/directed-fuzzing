#!/usr/bin/env python3

import requests
import pandas as pd

URL = "https://storage.googleapis.com/fuzzbench-data/2020-10-11-ossfuzzc/coverage/reports/{target}/{fuzzer}/index.html"

BENCHMARKS = [
    "bloaty_fuzz_target",
    "curl_curl_fuzzer_http",
    "freetype2-2017",
    "harfbuzz-1.3.2",
    "jsoncpp_jsoncpp_fuzzer",
    "lcms-2017-03-21",
    "libjpeg-turbo-07-2017",
    "libpcap_fuzz_both",
    "libpng-1.2.56",
    "libxslt_xpath",
    "mbedtls_fuzz_dtlsclient",
    "openssl_x509",
    "openthread-2019-12-23",
    "proj4-2017-08-14",
    "re2-2014-12-09",
    "systemd_fuzz-link-parser",
    "vorbis-2017-12-11",
    "woff2-2016-05-06",
    "zlib_zlib_uncompress_fuzzer"
]


def get_html(target='bloaty_fuzz_target', fuzzer='aflcc'):
    args = dict(target=target, fuzzer=fuzzer)
    r = requests.get(URL.format(**args))
    return r.text


def parse_html(**kwargs):
    df_list = pd.read_html(URL.format(**kwargs))
    df = df_list[0]
    df.columns = df.iloc[0]
    return df.drop(df.index[0])


def print_total(target='bloaty_fuzz_target', fuzzer='afl'):
    df = parse_html(target=benchmark, fuzzer='afl')
    df.iloc[-1]['Filename'] = target
    print(df.tail(1).to_markdown(index=False, headers={}))


for benchmark in BENCHMARKS:
    print_total(target=benchmark, fuzzer='afl')
