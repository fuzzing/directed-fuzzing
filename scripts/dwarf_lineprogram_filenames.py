#!/usr/bin/env python
# -------------------------------------------------------------------------------
# elftools example: dwarf_lineprogram_filenames.py
#
# In the .debug_line section, the Dwarf line program generates a matrix
# of address-source references. This example demonstrates accessing the state
# of each line program entry to retrieve the underlying filenames.
#
# William Woodruff (william@yossarian.net)
# This code is in the public domain
# -------------------------------------------------------------------------------
import argparse
import hashlib
import os
import sys
from elftools.elf.elffile import ELFFile


def process_file(filename, locations):
    with open(filename, 'rb') as f:
        elffile = ELFFile(f)

        if not elffile.has_dwarf_info():
            print('  file has no DWARF info')
            return

        dwarfinfo = elffile.get_dwarf_info()
        for CU in dwarfinfo.iter_CUs():

            # Every compilation unit in the DWARF information may or may not
            # have a corresponding line program in .debug_line.
            line_program = dwarfinfo.line_program_for_CU(CU)
            if line_program is None:
                print('  DWARF info is missing a line program for this CU')
                continue

            # Print a reverse mapping of filename -> #entries
            line_entry_mapping(line_program, locations)


def line_entry_mapping(line_program, locations):

    # The line program, when decoded, returns a list of line program
    # entries. Each entry contains a state, which we'll use to build
    # a reverse mapping of filename -> #entries.
    lp_entries = line_program.get_entries()
    for lpe in lp_entries:
        # We skip LPEs that don't have an associated file.
        # This can happen if instructions in the compiled binary
        # don't correspond directly to any original source file.
        if not lpe.state or lpe.state.file == 0:
            continue
        file_index = lpe.state.file
        lp_header = line_program.header
        file_entries = lp_header["file_entry"]

        # File and directory indices are 1-indexed.
        file_entry = file_entries[file_index - 1]
        filename = file_entry.name.decode()

        # Check if this is one of the locations we're looking for.
        key = (filename, lpe.state.line)
        if key in locations:
            print("found {}:{} = 0x{:x}".format(
                filename, lpe.state.line, lpe.state.address))

            locations[key]['addrs'].append(lpe.state.address)


def lpe_filename(line_program, file_index):
    # Retrieving the filename associated with a line program entry
    # involves two levels of indirection: we take the file index from
    # the LPE to grab the file_entry from the line program header,
    # then take the directory index from the file_entry to grab the
    # directory name from the line program header. Finally, we
    # join the (base) filename from the file_entry to the directory
    # name to get the absolute filename.
    lp_header = line_program.header
    file_entries = lp_header["file_entry"]

    # File and directory indices are 1-indexed.
    file_entry = file_entries[file_index - 1]
    dir_index = file_entry["dir_index"]

    # A dir_index of 0 indicates that no absolute directory was recorded during
    # compilation; return just the basename.
    if dir_index == 0:
        return file_entry.name.decode()

    directory = lp_header["include_directory"][dir_index - 1]
    return os.path.join(directory, file_entry.name).decode()


def binary_hash(filename):
    BLOCKSIZE = 2**16
    sha1sum = hashlib.sha1()
    with open(filename, 'rb') as fp:
        data = fp.read(BLOCKSIZE)
        while len(data) > 0:
            sha1sum.update(data)
            data = fp.read(BLOCKSIZE)
    return sha1sum.hexdigest()


def parse_args():
    parser = argparse.ArgumentParser(description="Exract addresses from binaries based on Dwarf info.")
    parser.add_argument('-f', '--targets', default=None, help='file with targets (filename:linenum)')
    parser.add_argument('-s', '--single', help='single target (filename:linenum) format')
    parser.add_argument('filename', help="compiled binary with debug information")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    locs = dict()
    if args.targets:
        with open(args.targets) as f:
            for line in f.readlines():
                src, num = line.strip().split(':')
                locs[(src, int(num))] = {'addrs': list()}
    elif args.single:
        src, num = args.single.split(':')
        locs[(src, int(num))] = {'addrs': list()}
    else:
        print("Error: you must provide a target file or a single target.'")
        sys.exit(1)
    filehash = binary_hash(args.filename)
    print(f"[*] Processing file: {args.filename}\n\t   sha1sum = {filehash}")  # noqa
    process_file(args.filename, locs)
    addrs = set([addr for v in locs.values() for addr in v['addrs']])
    print(sorted(addrs))
