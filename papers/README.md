

# SoK: The Progress, Challenges, and Perspectives of Directed Greybox Fuzzing (arXiv)
- [link](https://arxiv.org/abs/2005.11907)

<details>
  <summary> decent comparison table </summary>
  ![sok-table-1](images/sok-table-1.png)
</details>

<details>
  <summary>Comparison on the "user provided" time limit for the exploration phase in AFLGo.
  </summary>
  This experiment was done on _libxml2_ with a fuzzing experiment time of 24 hours.
  The number indicates the hours spent in the exploration phase and the y-axis is the 
  distance to target code areas.  No idea how target code areas were selected.

  ![sok-figure-3](images/aflgo-min-distance-figure-3.png)
</details>


# FuzzGuard (USENIX'20)
- [paper](https://www.usenix.org/system/files/sec20-zong.pdf), 
  [slides](https://www.usenix.org/system/files/sec20_slides_zong.pdf)
- GH repo, only contains [POC inputs](https://github.com/zongpy/FuzzGuard)
- Contributions:
    - first deep-learning-based solution to identify and remove unreachable inputs
- One feature vector is the set of 'pre-dominating nodes' + the target node.
- One feature vector is the input bytes.
- Defines a _seed similarity degree_ (SSD) to reduce the number of seeds that need to be sampled.
- In the evaluation, they merely run AFLGo once, then use the sequence of generated inputs and 
  calculate the time that would be saved by discarding inputs with their CNN model.


# Directed Greybox Fuzzing (CCS'17) AFLGo
- [abstract link](https://dl.acm.org/doi/abs/10.1145/3133956.3134020), 
  [paper download](https://www.comp.nus.edu.sg/~abhik/pdf/CCS17.pdf)
- GH [repo](https://github.com/aflgo/aflgo)
- Introduces (?) _exploration_ and _exploitation_ phases
    - The exploration phase (traditional coverage guided AFL).
    - The exploitation phase:
        - Transisition from exploration based on user provided time limit.
        - Most time is spend on seeds closest to the pre-determined targets.
        - A simulated annealing function slowly assigns more "energy" (time) to seeds that
          are closer to the target locations.
    - The LLVM instrumentation marks all sanitizer points (coverage) with the distance to 
      (??).  They have a hybrid method of computing CFG distance and combining with iCFG 
      distance.
- Compared performance to Katch on Heartbleed and binuilts/diffutils/findutils.
    - "AFLGo and Katch complement each other." venn diagram in paper.
    - AFLGo exposes heartbleed in 20m; Katch fails after 24h.
- Compared performance to BugRedux for 7 bugs.
- Conducted a sound evaluation of two targets with 10 CVEs to demonstrate
  AFLGo could find the CVE faster than AFL.
    - Improved find time in 8 cases, got worse in 2?
- Takes __forever__ to compile and calculated the distances.


# BugRedux (ISCE'12) GaTech
- [paper]()
- [repo](https://github.com/gatech/bugredux)
- Klee based

# Katch (FSE'13)
- [paper](https://srg.doc.ic.ac.uk/files/papers/katch-fse-13.pdf)
- [project-page](https://srg.doc.ic.ac.uk/projects/katch/)
- by [UK Software Reliability Group](https://srg.doc.ic.ac.uk/) / Cristian Cadar
- Contributions:
    - A technique for patch testing that combines symbolic execution with several 
    novel heuristics based on program analysis that effectively exploit the 
    program structure and existing program inputs.
    - A flexible system called katch, based on the stateof-the-art symbolic 
    execution engine klee that implements these techniques for real C programs.
- A patch testing tool:
    - For each test case input (in the test suite), katch computes an estimated 
      distance to the patch and then selects the closest input as the 
      starting point for symbolic exploration.
    - To reach the patch, katch employs three heuristics based on program analysis: 
      greedy exploration, informed path regeneration and definition switching
- Evaluated on GNU binutils, diffutils, and findutils (19 programs)
    - 125 patches in findutils
    - 175 patches in diffutils
    - 181 patches in binutils
    - timeout of either 10 or 15 minutes (4 cores, 16 GB of RAM)
    - Katch covered an additional 283 locations not already covered by
      the existing test suite, 656 remained uncovered.
- Artifact evaluation is available (https://srg.doc.ic.ac.uk/projects/katch/artifact.html)

# Sequence Directed Hybrid Fuzzing (SANER'20) _Berry_
- NOT RELEASED.
- Contributions:
    - A sequence directed hybrid fuzzing (SDHF) technique which combines 
      directed grey-box fuzzing and concolic execution, and guides them with user-specified statement sequences.
- Uses QSYM for concolic execution, but adjusts the parameters for when QSYM should attempt to find new inputs.
- Weak evaluation on LAVA-M.
- Repeated AFLGo (which was the BugRedux) evaluation. No improvement over AFLGo.

# Sequence coverage directed greybox fuzzing (ICPC'19) _LOLLY_
- NOT RELEASED
- Contributions:
    - SCDG  guided by user-specified statement sequences (instead of line #s, basic blocks.)
    - _novel_ energy schedule algorithm
- Built on AFL
- Evaluated on `libming` (7 CVEs)

# Hawkeye (CCS'18) NTU (Singapore)
- NOT RELEASED
- some implementation [details](https://sites.google.com/view/ccs2018-fuzz)
- related [FOT The Fuzzer](https://sites.google.com/view/fot-the-fuzzer)
- Evaluated on binutils (8 CVEs), MJS (4 bugs), Oniguruma (4 bugs), and Google FTS (3 targets).

# SeededFuzz: Selecting and Generating Seeds for Directed Fuzzing (TASE'16)
- Interesting title...
