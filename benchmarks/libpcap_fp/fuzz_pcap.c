#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "pcap/pcap.h"

#ifdef DFSAN
__attribute__((weak)) void dfsan_set_label_input(void *addr, size_t size);
__attribute__((weak)) void dfsan_set_label_harness(void *addr, size_t size);
#endif

static FILE * outfile = NULL;
static int fd = 0;
static char tmp_filename[32];

int LLVMFuzzerInitialize(int* argc, char*** argv) {
  //initialize output file
  if (outfile == NULL) {
      outfile = fopen("/dev/null", "w");
      if (outfile == NULL) {
          return 0;
      }
  }

  // initialize buffer file
  strncpy(tmp_filename, "/tmp/fuzz.pcap-XXXXXX", 31);
  fd = mkstemp(tmp_filename);
  if (fd < 0) {
    printf("failed open, errno=%d\n", errno);
    return -2;
  }
  return 0;
}

static inline int bufferToFile(const uint8_t *Data, size_t Size) {
    lseek(fd, 0, SEEK_SET);

    if (write(fd, Data, Size) != Size) {
        close(fd);
        return -3;
    }

    if (ftruncate(fd, Size)) {
       perror("failed to truncate");
       close(fd);
       return -3;
    }
    lseek(fd, 0, SEEK_SET);
    return 0;
}

void fuzz_openFile(const char * name) {
    if (outfile != NULL) {
        fclose(outfile);
    }
    outfile = fopen(name, "w");
}

int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
    pcap_t * pkts;
    char errbuf[PCAP_ERRBUF_SIZE];
    const u_char *pkt;
    struct pcap_pkthdr *header;
    struct pcap_stat stats;
    int r;

    if (Size < 1) {
        return 0;
    }
    //rewrite buffer to a file as libpcap does not have buffer inputs
    if (bufferToFile(Data, Size) < 0) {
        return 0;
    }

    //initialize structure
    pkts = pcap_open_offline(tmp_filename, errbuf);
    if (pkts == NULL) {
        fprintf(outfile, "Couldn't open pcap file %s\n", errbuf);
        return 0;
    }

    //loop over packets
    r = pcap_next_ex(pkts, &header, &pkt);
    while (r > 0) {
        //TODO pcap_offline_filter
        fprintf(outfile, "packet length=%d/%d\n",header->caplen, header->len);
        r = pcap_next_ex(pkts, &header, &pkt);
    }
    if (pcap_stats(pkts, &stats) == 0) {
        fprintf(outfile, "number of packets=%d\n", stats.ps_recv);
    }
    //close structure
    pcap_close(pkts);

    return 0;
}
