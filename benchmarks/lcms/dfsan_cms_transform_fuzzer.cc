// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdint.h>

#include <sanitizer/dfsan_interface.h>
extern dfsan_label __pllr_dfsan_harness_label;

#ifdef LABEL_INPUTS
extern dfsan_label __pllr_dfsan_input_label;
#endif

#include "lcms2.h"

#define FAST_16_LABELS

#ifdef FAST_16_LABELS
#define DFSAN_HAS_LABEL __has_label
static bool __has_label(dfsan_label label, dfsan_label elem) {
	return label & elem;
}

extern "C" void __dump_label(char *desc, void *data, size_t size) {
  dfsan_label Label = dfsan_read_label(data, size);
  fprintf(stderr, " %s label '%x'\n", desc, Label);
}

#else
extern "C" void printLabel(char * desc, long data) {
  dfsan_label Label = dfsan_get_label(data);
  const struct dfsan_label_info *LabelInfo = dfsan_get_label_info(Label);
  fprintf(stderr, " %s has label '%s'\n", desc, LabelInfo->desc);
}

extern "C" void __dump_label(char *desc, void *data, size_t size) {
  dfsan_label Label = dfsan_read_label(data, size);
  const struct dfsan_label_info *LabelInfo = dfsan_get_label_info(Label);
  fprintf(stderr, " %s has label '%s'\n", desc, LabelInfo->desc);
}
#endif

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
#ifdef LABEL_INPUTS
  dfsan_set_label(__pllr_dfsan_input_label, (void*)data, size);
  dfsan_set_label(__pllr_dfsan_input_label, &size, sizeof(size));
  fprintf(stderr, "  checking.. input=%d \t harness=%d\n", __pllr_dfsan_input_label, __pllr_dfsan_harness_label);
  fprintf(stderr, "             data =%d\n", dfsan_read_label(data, size));
#endif

  cmsHPROFILE srcProfile = cmsOpenProfileFromMem(data, size);
  if (!srcProfile) return 0;

  cmsHPROFILE dstProfile = cmsCreate_sRGBProfile();
  if (!dstProfile) {
    cmsCloseProfile(srcProfile);
    return 0;
  }

  cmsColorSpaceSignature srcCS = cmsGetColorSpace(srcProfile);
  cmsUInt32Number nSrcComponents = cmsChannelsOf(srcCS);
  cmsUInt32Number srcFormat;
  if (srcCS == cmsSigLabData) {
    srcFormat =
        COLORSPACE_SH(PT_Lab) | CHANNELS_SH(nSrcComponents) | BYTES_SH(0);
  } else {
    srcFormat =
        COLORSPACE_SH(PT_ANY) | CHANNELS_SH(nSrcComponents) | BYTES_SH(1);
  }

  cmsUInt32Number intent = INTENT_SATURATION;
  cmsUInt32Number flags = 0;

  dfsan_set_label(__pllr_dfsan_harness_label, &intent, sizeof(intent));
  dfsan_set_label(__pllr_dfsan_harness_label, &flags, sizeof(flags));

  __dump_label("srcCS", (void*) &srcCS, sizeof(srcCS));
  __dump_label("nSrcComponents", &nSrcComponents, sizeof(nSrcComponents));
  __dump_label("srcFormat", &srcFormat, sizeof(srcFormat));
  cmsHTRANSFORM hTransform = cmsCreateTransform(
      srcProfile, srcFormat, dstProfile, TYPE_BGR_8, intent, flags);

  fprintf(stderr, "[*] number of source tags = %u\n", cmsGetTagCount(srcProfile));
  cmsTagSignature srcTagSig = cmsGetTagSignature(srcProfile, 7);
  fprintf(stderr, "srcTagSig = %x (%s)\t", srcTagSig, &srcTagSig);
  __dump_label("srcTagSig", &srcTagSig, sizeof(srcTagSig));

  cmsUInt32Number srcVer = cmsGetEncodedICCversion(srcProfile);
  __dump_label("srcVer", &srcVer, sizeof(srcVer));

  cmsCloseProfile(srcProfile);
  cmsCloseProfile(dstProfile);
  if (!hTransform) return 0;

  uint8_t output[4];
  if (T_BYTES(srcFormat) == 0) {  // 0 means double
    double input[nSrcComponents];
    for (uint32_t i = 0; i < nSrcComponents; i++) input[i] = 0.5f;
    cmsDoTransform(hTransform, input, output, 1);
  } else {
    uint8_t input[nSrcComponents];
    for (uint32_t i = 0; i < nSrcComponents; i++) input[i] = 128;
    cmsDoTransform(hTransform, input, output, 1);
  }
  cmsDeleteTransform(hTransform);
  
  dfsan_label Label = dfsan_get_label(intent);
  const struct dfsan_label_info *LabelInfo = dfsan_get_label_info(Label);
  if (DFSAN_HAS_LABEL(Label, __pllr_dfsan_harness_label)) {
    fprintf(stderr, " intent has label '%s'\n", LabelInfo->desc);
  }
//printLabel("__CS", srcCS);
//printLabel("__nSrcComponents", nSrcComponents);


  return 0;
}
