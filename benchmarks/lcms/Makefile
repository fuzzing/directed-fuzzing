VERSION ?= lcms2.10
L_VERSION = Little-CMS-$(VERSION)

all: $(L_VERSION)/configure $(FUZZER_CC)

include ../config.mk


FUZZER_CC = cms_transform_fuzzer.cc
STATIC_LIB = $(VERSION)/src/.libs/liblcms2.a
CONFIGURE_OPTIONS = --disable-shared --enable-static



$(LIB_AFLPP):
	cd $(AFLPP_HOME) && ar r FuzzingEngine.a utils/aflpp_driver/aflpp_driver.o afl-compiler-rt-64.o

$(L_VERSION):
	wget -qO- https://github.com/mm2/Little-CMS/archive/$(VERSION).tar.gz | tar xz

$(L_VERSION)/configure: $(L_VERSION)
	autoreconf -fi $<

cms_transform_fuzzer.cc:
	wget -qO $@ 'https://raw.githubusercontent.com/google/fuzzbench/master/benchmarks/lcms-2017-03-21/cms_transform_fuzzer.cc'



cov-$(VERSION)/Makefile:
	mkdir cov-$(VERSION)
	cd cov-$(VERSION) && \
	CFLAGS="$(CFLAGS)" \
	LDFLAGS="$(LDFLAGS)" \
	../$(L_VERSION)/configure $(CONFIGURE_OPTIONS)

cov-$(STATIC_LIB) : cov-$(VERSION)/Makefile
	$(MAKE) -C cov-$(VERSION)

cov-$(VERSION)/fuzzer : cov-$(STATIC_LIB) $(FUZZER_CC)
	$(CXX) $(CXXFLAGS) \
		-I$(L_VERSION)/include $(FUZZER_CC) -o $@ $< $(LIB_AFLPP)


pgo-$(VERSION)/Makefile:
	mkdir pgo-$(VERSION)
	cd pgo-$(VERSION) && \
	CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)" \
	../$(L_VERSION)/configure $(CONFIGURE_OPTIONS)

pgo-$(STATIC_LIB) : pgo-$(VERSION)/Makefile profile.profdata
	$(MAKE) -C pgo-$(VERSION)

pgo-$(VERSION)/fuzzer.pre.bc : pgo-$(STATIC_LIB)
	$(CXX) $(CXXFLAGS) $(FLTO_EMIT_LLVM) \
		-I$(L_VERSION)/include $(FUZZER_CC) -o $@ $< $(LIB_AFLPP)

pgo-$(VERSION)/fuzzer.bc : pgo-$(VERSION)/fuzzer.pre.bc
	$(OPT) -load $(EDGE_IDS_PASS) -assign-edge-ids -o $@ $< >/dev/null


dfsan-$(VERSION)/Makefile:
	mkdir dfsan-$(VERSION)
	cd dfsan-$(VERSION) && \
	CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)" \
	../$(L_VERSION)/configure $(CONFIGURE_OPTIONS)

dfsan-$(STATIC_LIB) : dfsan-$(VERSION)/Makefile
	$(MAKE) -C dfsan-$(VERSION)

dfsan-$(VERSION)/dfsan_fuzzer : dfsan-$(STATIC_LIB)
	$(CXX) $(CXXFLAGS) -DLABEL_INPUTS -DFAST_16_LABELS \
		-I$(L_VERSION)/include dfsan_$(FUZZER_CC) -o $@ $< $(LIB_AFLPP) $(DFSAN_RT)

dfsan-$(VERSION)/fuzzer : pllr-$(VERSION)/fuzzer.bc $(PATH_LLR_RT) $(LIB_AFLPP)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -DLABEL_INPUTS -DFAST_16_LABELS -o $@ $^


pllr-$(VERSION)/fuzzer.bc : pgo-$(VERSION)/fuzzer.bc
	@mkdir -p $(@D)
	$(OPT) -load $(PATH_LLR_PASS) -path-llr -o $@ $< >/dev/null

pllr-$(VERSION)/fuzzer : pllr-$(VERSION)/fuzzer.bc $(LIB_AFLPP) $(PATH_LLR_RT)
	$(CXX) -g -stdlib=libc++ -o $@ $^ -lm


aflpp-$(VERSION)/Makefile:
	mkdir aflpp-$(VERSION)
	cd aflpp-$(VERSION) && \
	CFLAGS="-g -w" \
	../$(L_VERSION)/configure $(CONFIGURE_OPTIONS) CC=$(CC)

aflpp-$(STATIC_LIB) : aflpp-$(VERSION)/Makefile
	$(MAKE) -C aflpp-$(VERSION)

aflpp-$(VERSION)/fuzzer : aflpp-$(STATIC_LIB) $(FUZZER_CC)
	$(CXX) $(CXXFLAGS) \
		-I$(L_VERSION)/include $(FUZZER_CC) -o $@ $< $(LIB_AFLPP)


cmplog-$(VERSION)/Makefile:
	mkdir cmplog-$(VERSION)
	cd cmplog-$(VERSION) && \
	CFLAGS="-g -w" \
	AFL_LLVM_CMPLOG=1 \
	../$(L_VERSION)/configure $(CONFIGURE_OPTIONS) CC=$(CC)

cmplog-$(STATIC_LIB) : cmplog-$(VERSION)/Makefile
	AFL_LLVM_CMPLOG=1 \
	$(MAKE) -C cmplog-$(VERSION)

cmplog-$(VERSION)/fuzzer : cmplog-$(STATIC_LIB) $(FUZZER_CC)
	AFL_LLVM_CMPLOG=1 \
	$(CXX) $(CXXFLAGS) \
		-I$(L_VERSION)/include $(FUZZER_CC) -o $@ $< $(LIB_AFLPP)
