#!/bin/bash -eu

export CFLAGS="-DCHECK_ELF -DCHECK_JSON -DCHECK_IMAGE -DSTBI_ONLY_PNG"

build_fuzz() {
    rm -rf cov-*
    rm -rf aflpp-*
    rm -rf cmplog-*
    rm -f bin/*
    
    make cov
    make aflpp
    make cmplog
    make install
    cp -a cmplog-*/fuzzer bin/cmplog_fuzzer
}

build_pllr() {
  rm -rf pgo-*
  rm -rf pllr-*
  ln -f $1 profile.profdata
  make pgo
  make pllr
  mkdir -p "$2"
  cp -a $1 ${2}/$1
  cp -a pllr-checker-v0/fuzzer $2/pllr_fuzzer
  cp -a pllr-checker-v0/fuzzer.bc $2/pllr_fuzzer.bc
  cp -a pgo-checker-v0/fuzzer.bc $2/pgo_fuzzer.bc
}

if [[ $# == 0 ]]; then
    echo "[*] building binaries for fuzzing."
    build_fuzz
elif [[ $# == 2 && -e $1 ]]; then
    echo "[*] building Path-LLR binary and saving in '$2'."
    build_pllr $1 $2
else 
    echo "usage ./$0 [ <profdata_file> <export_directory> ]"
fi
