#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "checker.h"
#include "elf.h"
#include "json.h"
#include "json-builder.h"

#ifdef CHECK_IMAGE
  #define STB_IMAGE_IMPLEMENTATION
  #include "stb_image.h"
#endif

static int memfd_create_syscall(const char *name, unsigned int flags)
{
    int fd;
    fd = syscall(SYS_memfd_create, name, flags);
    if (fd == -1) {
        if(errno == ENOSYS) fprintf(stderr, "SYS_memfd_create not supported on this kernel");
        exit(EXIT_FAILURE);

    }
    return fd;
}

int write_to_file(const uint8_t *buf, size_t len) {
  int fd = memfd_create_syscall("cur_input", 0);
  if (write(fd, buf, len) < 0) {
    return -1;
  }
  return fd;
}

int check_elf_file(int fd) {
  struct Fhdr elf_header;
  FILE *f = fdopen(fd, "r");
  return readelf(f, &elf_header);
}

int check_json_data(const uint8_t *buf, size_t len) {

  json_char *json_sbuf;
  char json_error[128];
  json_char* json = (json_char *)buf;
  json_settings settings = {};
  settings.value_extra = json_builder_extra;

  struct json_serialize_opts json_sopts;
  json_sopts.mode = json_serialize_mode_packed;
  json_sopts.opts = json_serialize_opt_pack_brackets |
                    json_serialize_opt_no_space_after_comma;
  json_sopts.indent_size = 0;
 
  json_value *value = json_parse_ex(&settings, json, len, json_error);

  if (value == NULL) {
    fprintf(stderr, "[-] failed to parse json data\n");
    return 0; 
  } else if (len < 4096) {
    json_sbuf = malloc(json_measure(value));
    json_serialize_ex(json_sbuf, value, json_sopts);
    free(json_sbuf);
  }
  
  if (value) {
    json_builder_free(value);
  }
  return 1;
}

#ifdef CHECK_IMAGE
int check_image(const uint8_t *buf, size_t len) {
  int ret = 0;
  int x, y, channels;

  if(!stbi_info_from_memory(buf, len, &x, &y, &channels)) return 0;

  /* exit if the image is larger than ~40MB */
  if(y && x > (40000000 / 4) / y) return 0;

  unsigned char *img = stbi_load_from_memory(buf, len, &x, &y, &channels, 4);

  stbi_image_free(img);
  return ret;
}
#endif


