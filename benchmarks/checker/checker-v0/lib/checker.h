#include <stdint.h>
#include <unistd.h>

#ifdef __cplusplus
   extern "C"
   {

#endif

int write_to_file(const uint8_t*, size_t);
int check_elf_file(int);
int check_json_data(const uint8_t*, size_t);
int check_image(const uint8_t*, size_t);

#ifdef __cplusplus
   } /* extern "C" */
#endif

