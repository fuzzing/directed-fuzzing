#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "lib/checker.h"

// fuzz_target.cc
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  int fd = 0;
#if defined CHECK_ELF || CHECK_JSON || CHECK_IMAGE
  int ret = 0;
#endif

#if defined CHECK_ELF
  if (Data[0] != '{' && Data[1] == 'E' && Data[2] == 'L' && Data[3] == 'F') {
    if ((fd = write_to_file(Data, Size)) > 0) {
      fprintf(stderr, "checking elf\n");
      ret = check_elf_file(fd);
    }
  }
#endif

#if defined CHECK_JSON
  if (Size < 4096*4 && Data[0] == '{' && Data[1] == 10) {
      fprintf(stderr, "checking json\n");
    ret = check_json_data(Data, Size);
  }
#endif

#ifdef CHECK_IMAGE
  if (Data[0] == 137 && Data[1] == 'P' && Data[2] == 'N' && Data[3] == 'G') {
    fprintf(stderr, "checking image\n");
    check_image(Data, Size);
  }
#endif

  if (fd) {
    close(fd);
  }
  return 0;  // Non-zero return values are reserved for future use.
}
