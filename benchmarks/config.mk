
export CC = /usr/lib/llvm-12/bin/clang
export CXX = /usr/lib/llvm-12/bin/clang++
export AR = /usr/lib/llvm-12/bin/llvm-ar
export AS = /usr/lib/llvm-12/bin/llvm-as
export RANLIB = /usr/lib/llvm-12/bin/llvm-ranlib
export LD = /usr/lib/llvm-12/bin/ld.lld
export LLVM_SYMBOLIZER_PATH = /usr/lib/llvm-12/bin/llvm-symbolizer
OPT = /usr/lib/llvm-12/bin/opt
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ABILIST := $(mkfile_path:config.mk=abilist.txt)

.DEFAULT_GOAL := all

USE_COV = -fprofile-instr-generate -fcoverage-mapping
USE_PGO = -fprofile-instr-use="$(shell pwd)/profile.profdata"
DFSAN_EVENT_CB = -mllvm -dfsan-event-callbacks
DFSAN_ABILIST = -mllvm -dfsan-abilist=/usr/lib/llvm-12/lib/clang/12.0.1/share/dfsan_abilist.txt
CUSTOM_ABILIST = -mllvm -dfsan-abilist=${ABILIST}
DFSAN_FAST16 = -mllvm -dfsan-fast-16-labels
DFSAN_ABI = -mllvm -dfsan-args-abi=false
USE_DFSAN = -fsanitize=dataflow $(DFSAN_ABILIST) $(CUSTOM_ABILIST) $(DFSAN_ABI) $(DFSAN_FAST16) $(DFSAN_EVENT_CB) -DDFSAN
FLTO_EMIT_LLVM = -Wl,-plugin-opt=emit-llvm

AFLPP_HOME=/opt/aflpp
LIB_AFL = AFL/FuzzingEngine.a
LIB_AFLPP = /opt/aflpp/FuzzingEngine.a
DFSAN_LIB_AFLPP = /opt/aflpp/FuzzingEngineDFSan.a

EDGE_IDS_PASS = ../../build_pass/cfg_pass/libAssignEdges.so
PATH_LLR_PASS = ../../build_pass/path_llr_pass/libPathLLR.so
PATH_LLR_RT = ../../build_pass/path_llr_pass/libPathLLR-Runtime.a
DFSAN_RT = ../../build_pass/path_llr_pass/libDFSan-Runtime.a

cov : CFLAGS = -g -w -flto $(USE_COV)
cov : CXXFLAGS = $(CFLAGS) -std=c++11 -stdlib=libc++
cov : LDFLAGS = -flto -fuse-ld=gold
cov : cov-$(VERSION)/fuzzer

pgo : CFLAGS = -g -w -flto $(USE_PGO)
pgo : CXXFLAGS = $(CFLAGS) -std=c++11 -stdlib=libc++
pgo : LDFLAGS = -flto -fuse-ld=gold
pgo : pgo-$(VERSION)/fuzzer.bc

pllr : pllr-$(VERSION)/fuzzer

aflpp cmplog : CC = $(AFLPP_HOME)/afl-clang-lto
aflpp cmplog : CXX = $(AFLPP_HOME)/afl-clang-lto++
aflpp cmplog : CXXFLAGS = $(CFLAGS) -std=c++11 -stdlib=libc++
aflpp cmplog : LIB_AFLPP = $(AFLPP_HOME)/utils/aflpp_driver/libAFLDriver.a
aflpp : aflpp-$(VERSION)/fuzzer
cmplog : cmplog-$(VERSION)/fuzzer

bin/aflpp_fuzzer : aflpp-$(VERSION)/fuzzer
	@mkdir -p $(@D)
	cp $< $@

bin/cov_fuzzer : cov-$(VERSION)/fuzzer
	@mkdir -p $(@D)
	cp $< $@

bin/cmplog_fuzzer : cmplog-$(VERSION)/fuzzer
	@mkdir -p $(@D)
	cp $< $@

install : bin/aflpp_fuzzer bin/cov_fuzzer bin/cmplog_fuzzer seeds

dfsan : CFLAGS = -g -w  $(USE_DFSAN)
dfsan : CXXFLAGS = $(CFLAGS) -std=c++11 -stdlib=libc++
# dfsan : dfsan-$(VERSION)/dfsan_fuzzer
dfsan : dfsan-$(VERSION)/fuzzer

xray : CFLAGS = -g -w -flto -fxray-instrument -fxray-instruction-threshold=1
xray : CXXFLAGS = $(CFLAGS) -fuse-ld=gold
xray : LDFLAGS = -flto -fuse-ld=gold
xray : xray-$(VERSION)/fuzzer
