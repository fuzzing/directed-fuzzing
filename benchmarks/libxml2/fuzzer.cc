// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <string>
#include <vector>
#include "libxml/xmlversion.h"
#include "libxml/parser.h"
#include "libxml/HTMLparser.h"
#include "libxml/tree.h"

void ignore (void * ctx, const char * msg, ...) {}

#ifdef DFSAN
extern "C" __attribute__((weak)) void dfsan_set_label_input(void *addr, size_t size);
extern "C" __attribute__((weak)) void dfsan_set_label_harness(void *addr, size_t size);
#endif

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  int options = 0;
  const char URL[] = "noname.xml";
  const char *encoding = NULL;
#ifdef DFSAN
  dfsan_set_label_harness(&options, sizeof(int));
  dfsan_set_label_harness((void*)URL, sizeof(URL));
  dfsan_set_label_harness(&encoding, sizeof(void*));
#endif

  xmlSetGenericErrorFunc(NULL, &ignore);
  if (auto doc = xmlReadMemory(reinterpret_cast<const char *>(data), size,
                               URL, encoding, options))
    xmlFreeDoc(doc);
 
  return 0;
}
