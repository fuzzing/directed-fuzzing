#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "pcap/pcap.h"

#ifdef DFSAN
__attribute__((weak)) void dfsan_set_label_input(void *addr, size_t size);
__attribute__((weak)) void dfsan_set_label_harness(void *addr, size_t size);
#endif

static FILE * outfile = NULL;
static int fd = 0;
static char tmp_filename[32];

int LLVMFuzzerInitialize(int* argc, char*** argv) {
  //initialize output file
  if (outfile == NULL) {
      outfile = fopen("/dev/null", "w");
      if (outfile == NULL) {
          return 0;
      }
  }

  // initialize buffer file
  strncpy(tmp_filename, "/tmp/fuzz.pcap-XXXXXX", 31);
  fd = mkstemp(tmp_filename);
  if (fd < 0) {
    printf("failed open, errno=%d\n", errno);
    return -2;
  }
  return 0;
}

static inline int bufferToFile(const uint8_t *Data, size_t Size) {
    lseek(fd, 0, SEEK_SET);

    if (write(fd, Data, Size) != Size) {
        close(fd);
        return -3;
    }

    if (ftruncate(fd, Size)) {
       perror("failed to truncate");
       close(fd);
       return -3;
    }
    lseek(fd, 0, SEEK_SET);
    return 0;
}

void fuzz_openFile(const char * name) {
    if (outfile != NULL) {
        fclose(outfile);
    }
    outfile = fopen(name, "w");
}

int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
    pcap_t * pkts;
    char errbuf[PCAP_ERRBUF_SIZE];
    const u_char *pkt;
    struct pcap_pkthdr *header;
    int r;
    size_t filterSize;
    char * filter;
    struct bpf_program bpf;


    if (Size < 1) {
        return 0;
    }
    filterSize = Data[0];
    if (Size < 1+filterSize || filterSize == 0) {
        return 0;
    }

    //rewrite buffer to a file as libpcap does not have buffer inputs
    if (bufferToFile(Data+1+filterSize, Size-(1+filterSize)) < 0) {
        return 0;
    }

#ifdef DFSAN
    dfsan_set_label_input(tmpfile, sizeof(tmpfile));
#endif

    //initialize structure
    pkts = pcap_open_offline(tmp_filename, errbuf);
    if (pkts == NULL) {
        fprintf(outfile, "Couldn't open pcap file %s\n", errbuf);
        return 0;
    }

    filter = (char*)malloc(filterSize);
    memcpy(filter, Data+1, filterSize);
    //null terminate string
    filter[filterSize-1] = 0;

#ifdef DFSAN
    dfsan_set_label_input(&filter, filterSize);
#endif

    if (pcap_compile(pkts, &bpf, filter, 1, PCAP_NETMASK_UNKNOWN) == 0) {
        //loop over packets
        r = pcap_next_ex(pkts, &header, &pkt);
        while (r > 0) {
            //checks filter
            fprintf(outfile, "packet length=%d/%d filter=%d\n",header->caplen, header->len, pcap_offline_filter(&bpf, header, pkt));
            r = pcap_next_ex(pkts, &header, &pkt);
        }
        //close structure
        pcap_close(pkts);
        pcap_freecode(&bpf);
    }
    else {
        pcap_close(pkts);
    }
    free(filter);

    return 0;
}
